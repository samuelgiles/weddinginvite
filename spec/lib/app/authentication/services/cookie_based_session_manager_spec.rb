# frozen_string_literal: true

require 'rails_helper'

module App
  module Authentication
    module Services
      describe CookieBasedSessionManager do
        let(:service) { described_class.new(cookies) }

        let(:cookies) { instance_double(ActionDispatch::Cookies::CookieJar) }

        it_behaves_like 'it implements', WeddingInvite::Authentication::Interfaces::SessionManager

        describe '#use_login_token' do
          subject(:use_login_token) { service.use_login_token('super-secret') }

          let(:encrypted_cookies) do
            instance_double(ActionDispatch::Cookies::EncryptedKeyRotatingCookieJar)
          end
          let(:permanent_cookies) do
            instance_double(ActionDispatch::Cookies::PermanentCookieJar)
          end

          before do
            expect(cookies).to receive(:encrypted).and_return(encrypted_cookies)
            expect(encrypted_cookies).to receive(:permanent).and_return(permanent_cookies)
          end

          specify do
            expect(permanent_cookies).to receive(:[]=).with(:login_token, 'super-secret')
            use_login_token
          end
        end

        describe '#current_user' do
          subject(:current_user) { service.current_user }

          let(:encrypted_cookies) do
            instance_double(ActionDispatch::Cookies::EncryptedKeyRotatingCookieJar)
          end
          let(:permanent_cookies) do
            instance_double(ActionDispatch::Cookies::PermanentCookieJar)
          end

          before do
            expect(cookies).to receive(:encrypted).and_return(encrypted_cookies)
            expect(encrypted_cookies).to receive(:permanent).and_return(permanent_cookies)
            expect(permanent_cookies)
              .to receive(:[])
              .with(:login_token)
              .and_return(login_token_in_cookies)
          end

          context 'when there is a login token present in the users cookies' do
            let(:login_token_in_cookies) { 'super-secret' }
            let(:login_session_relation) { instance_double(ActiveRecord::Relation) }

            before do
              expect(LoginSession).to receive(:valid).and_return(login_session_relation)
              expect(login_session_relation)
                .to receive(:find_by)
                .with(token: 'super-secret')
                .and_return(found_login_session)
            end

            context 'when the login session matching the token can be found' do
              let(:found_login_session) { instance_double(LoginSession) }
              let(:person) { instance_double(Person) }
              let(:person_entity_builder) { instance_double(Builders::Person) }
              let(:person_entity) { instance_double(WeddingInvite::Entities::Person) }

              before do
                expect(found_login_session).to receive(:person).and_return(person)
                expect(Builders::Person)
                  .to receive(:new)
                  .with(person)
                  .and_return(person_entity_builder)
                expect(person_entity_builder).to receive(:build).and_return(person_entity)
              end

              it { is_expected.to be_some_value(person_entity) }
            end

            context 'when the login session matching the token cannot be found' do
              let(:found_login_session) { nil }

              it { is_expected.to be_a_none }
            end
          end

          context 'when there is not a login token in the users cookies' do
            let(:login_token_in_cookies) { nil }

            it { is_expected.to be_a_none }
          end
        end

        describe '#current_wedding' do
          subject(:current_wedding) { service.current_wedding }

          let(:encrypted_cookies) do
            instance_double(ActionDispatch::Cookies::EncryptedKeyRotatingCookieJar)
          end
          let(:permanent_cookies) do
            instance_double(ActionDispatch::Cookies::PermanentCookieJar)
          end

          before do
            expect(cookies).to receive(:encrypted).and_return(encrypted_cookies)
            expect(encrypted_cookies).to receive(:permanent).and_return(permanent_cookies)
            expect(permanent_cookies)
              .to receive(:[])
              .with(:login_token)
              .and_return(login_token_in_cookies)
          end

          context 'when there is a login token present in the users cookies' do
            let(:login_token_in_cookies) { 'super-secret' }
            let(:login_session_relation) { instance_double(ActiveRecord::Relation) }

            before do
              expect(LoginSession).to receive(:valid).and_return(login_session_relation)
              expect(login_session_relation)
                .to receive(:find_by)
                .with(token: 'super-secret')
                .and_return(found_login_session)
            end

            context 'when the login session matching the token can be found' do
              let(:found_login_session) { instance_double(LoginSession) }
              let(:person) { instance_double(Person) }
              let(:couple) { instance_double(Couple) }
              let(:wedding) { instance_double(Wedding) }
              let(:wedding_entity_builder) { instance_double(Builders::Wedding) }
              let(:wedding_entity) { instance_double(WeddingInvite::Entities::Wedding) }

              before do
                expect(found_login_session).to receive(:person).and_return(person)
                expect(person).to receive(:couple).and_return(couple)
                expect(couple).to receive(:wedding).and_return(wedding)
                expect(Builders::Wedding)
                  .to receive(:new)
                  .with(wedding)
                  .and_return(wedding_entity_builder)
                expect(wedding_entity_builder).to receive(:build).and_return(wedding_entity)
              end

              it { is_expected.to be_some_value(wedding_entity) }
            end

            context 'when the login session matching the token cannot be found' do
              let(:found_login_session) { nil }

              it { is_expected.to be_a_none }
            end
          end

          context 'when there is not a login token in the users cookies' do
            let(:login_token_in_cookies) { nil }

            it { is_expected.to be_a_none }
          end
        end
      end
    end
  end
end
