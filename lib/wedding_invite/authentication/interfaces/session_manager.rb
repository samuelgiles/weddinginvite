# frozen_string_literal: true

require 'duckface'

module WeddingInvite
  module Authentication
    module Interfaces
      module SessionManager
        extend Duckface::ActsAsInterface

        def use_login_token(_login_token)
          raise NotImplementedError
        end

        # Should return a Maybe type
        def current_user
          raise NotImplementedError
        end

        def current_wedding
          raise NotImplementedError
        end
      end
    end
  end
end
