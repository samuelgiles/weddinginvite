# frozen_string_literal: true

require 'app/signup/forms/stage_one'
require 'dry/matcher/result_matcher'

module App
  module Signup
    module Factories
      class StageOneForm
        def initialize(signup_persistence)
          @signup_persistence = signup_persistence
        end

        def manufacture
          Dry::Matcher::ResultMatcher.call(load_stage_one_result) do |matcher|
            matcher.success do |existing_stage_one|
              Forms::StageOne.new(
                params: StageOneParamsSerializer.new(existing_stage_one).to_h
              )
            end

            matcher.failure do |_|
              Forms::StageOne.new
            end
          end
        end

        private

        def load_stage_one_result
          @load_stage_one_result ||= @signup_persistence.load_stage_one
        end

        class StageOneParamsSerializer
          def initialize(stage_one)
            @stage_one = stage_one
          end

          def to_h
            @stage_one.attributes
          end
        end
      end
    end
  end
end
