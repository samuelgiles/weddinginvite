# frozen_string_literal: true

require 'wedding_invite/signup/validators/user_fills_in_stage_two_parameters'
require 'wedding_invite/signup/use_cases/user_fills_in_stage_two'
require 'wedding_invite/signup/interfaces/persistence'

module WeddingInvite
  module Signup
    module Validators
      describe UserFillsInStageTwoParameters do
        let(:validator) do
          described_class.new(
            user_fills_in_stage_two_parameters,
            signup_persistence: signup_persistence
          )
        end
        let(:user_fills_in_stage_two_parameters) do
          instance_double(
            UseCases::UserFillsInStageTwo::Parameters,
            name: 'Giles Wedding',
            domain: 'gileswedding'
          )
        end
        let(:signup_persistence) { instance_double(Interfaces::Persistence) }

        specify do
          expect(described_class).to have_validator(
            ActiveModel::Validations::PresenceValidator,
            %i[name domain]
          )
        end

        describe '#ensure_domain_is_available' do
          subject(:result) { validator.result }

          before do
            expect(signup_persistence)
              .to receive(:domain_is_available?)
              .with('gileswedding')
              .and_return(domain_is_available)
          end

          context 'when the domain is available' do
            let(:domain_is_available) { true }

            it { is_expected.to be_a_success_with_value(user_fills_in_stage_two_parameters) }
          end

          context 'when the domain is not available' do
            let(:domain_is_available) { false }

            specify do
              expect(result.failure.full_messages).to contain_exactly(
                "Domain isn't available"
              )
            end
          end
        end
      end
    end
  end
end
