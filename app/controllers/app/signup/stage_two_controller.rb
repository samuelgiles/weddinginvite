# frozen_string_literal: true

require 'app/signup/factories/stage_two_form'

module App
  module Signup
    class StageTwoController < AbstractSignupController
      def new
        @form = Factories::StageTwoForm.new(signup_persistence).manufacture
      end

      def create
        Dry::Matcher::ResultMatcher.call(user_fills_in_stage_two_result) do |matcher|
          matcher.success do |_stage_two|
            redirect_to new_app_signup_stage_three_path
          end

          matcher.failure do |errors|
            @form = populated_stage_two_form.with_errors(errors)
            render :new
          end
        end
      end

      private

      PERMITTED_CREATE_ATTRIBUTES = %i[
        name
        domain
      ]

      def create_params
        params.require(:app_signup_forms_stage_two).permit(*PERMITTED_CREATE_ATTRIBUTES)
      end

      def user_fills_in_stage_two_parameters
        populated_stage_two_form.to_parameter_object
      end

      def populated_stage_two_form
        @populated_stage_two_form ||= Forms::StageTwo.new(params: create_params.to_h)
      end

      def user_fills_in_stage_two_result
        WeddingInvite::Signup::UseCases::UserFillsInStageTwo.new(
          user_fills_in_stage_two_parameters,
          signup_persistence
        ).result
      end
    end
  end
end
