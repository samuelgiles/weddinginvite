# frozen_string_literal: true

require 'rails_helper'

module App
  describe AppController do
    describe '#index' do
      subject(:make_request) { get :index }

      before do
        expect(controller)
          .to receive(:logged_in?)
          .and_return(user_is_logged_in)
      end

      context 'when user is logged in' do
        let(:user_is_logged_in) { true }
        
        specify do
          make_request
          expect(response).to redirect_to(app_setup_path)
        end
      end

      context 'when user is logged out' do
        let(:user_is_logged_in) { false }

        specify do
          make_request
          expect(response).to be_successful
        end
      end
    end
  end
end
