# frozen_string_literal: true

require 'wedding_invite/types'
require 'dry/struct'

module WeddingInvite
  module Messaging
    module Entities
      class Message < Dry::Struct::Value
        attribute :phone_number, Types::Strict::String
        attribute :content, Types::Strict::String
      end
    end
  end
end
