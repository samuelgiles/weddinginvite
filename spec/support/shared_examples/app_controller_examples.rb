# frozen_string_literal: true

shared_examples_for 'a controller action that requires users to be logged in' do
  context 'when logged out' do
    before do
      expect(controller).to receive(:logged_in?).and_return(false)
    end

    specify do
      make_request
      expect(flash[:error]).to eq 'You need to be logged in to continue'
      expect(response).to redirect_to new_app_session_path
    end
  end
end
