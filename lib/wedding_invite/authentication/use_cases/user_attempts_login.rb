# frozen_string_literal: true

require 'wedding_invite/types'
require 'wedding_invite/messaging/entities/message'
require 'wedding_invite/authentication/validators/user_attempts_login_parameters'
require 'clean/abstract_use_case'

module WeddingInvite
  module Authentication
    module UseCases
      # The user provides a mobile phone number, we check whether it exists and if it does
      # we send them an authentication token (4 digits), if not we fail
      class UserAttemptsLogin < Clean::AbstractUseCase
        class Parameters < Clean::ParameterObject
          attribute :phone_number, Types::Strict::String.meta(omittable: true)
        end

        def initialize(parameters, authentication_persistence, messaging_service_provider)
          @parameters = parameters
          @authentication_persistence = authentication_persistence
          @messaging_service_provider = messaging_service_provider
        end

        def result
          validator.result.bind do |_|
            @messaging_service_provider.send_message(authentication_message)
            Dry::Monads::Success(@parameters.phone_number)
          end
        end

        private

        def authentication_message
          Messaging::Entities::Message.new(
            phone_number: @parameters.phone_number,
            content: authentication_message_content
          )
        end

        def authentication_message_content
          "💌 Your WeddingInvite.io verification code is #{authentication_code}"
        end

        def authentication_code
          @authentication_persistence.authentication_code_for(@parameters.phone_number)
        end

        def validator
          @validator ||= Validators::UserAttemptsLoginParameters.new(
            @parameters,
            authentication_persistence: @authentication_persistence
          )
        end
      end
    end
  end
end
