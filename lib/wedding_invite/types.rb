# frozen_string_literal: true

require 'dry-types'

module WeddingInvite
  module Types
    include Dry::Types.module
  end
end
