# frozen_string_literal: true

module App
  module Signup
    module Commands
      # Serializes stage two into the session
      class StoreStageTwoInSession
        def initialize(session, stage_two)
          @session = session
          @stage_two = stage_two
        end

        STAGE_SESSION_KEY = :signup_stage_two

        def execute
          @session[STAGE_SESSION_KEY] = StageTwoSerializer.new(@stage_two).to_h
        end

        private

        # Produces a hash suitable for storing stage two in the session
        class StageTwoSerializer
          extend Forwardable

          def initialize(stage_two)
            @stage_two = stage_two
          end

          def to_h
            {
              name: name,
              domain: domain
            }
          end

          private

          def_delegators :@stage_two,
                         :name,
                         :domain
        end
      end
    end
  end
end
