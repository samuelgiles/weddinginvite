# frozen_string_literal: true

require "dry/matcher/result_matcher"

module App
  class SetupController < BaseController
    requires_user_to_be_logged_in
    uses_navigation_screen

    def show
      @setup_dashboard = Presenters::SetupDashboard.new(current_wedding)
    end
  end
end
