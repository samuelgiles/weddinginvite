# frozen_string_literal: true

class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.datetime :starts_at, null: false
      t.references :venue, foreign_key: true
      t.string :details, null: false

      t.timestamps
    end
  end
end
