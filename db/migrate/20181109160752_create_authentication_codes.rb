# frozen_string_literal: true

class CreateAuthenticationCodes < ActiveRecord::Migration[5.2]
  def change
    create_table :authentication_codes do |t|
      t.string :phone_number, null: false
      t.boolean :expired, null: false, default: false
      t.string :code, null: false

      t.timestamps
    end
    add_index :authentication_codes, [:phone_number, :code]
    add_index :authentication_codes, :expired
  end
end
