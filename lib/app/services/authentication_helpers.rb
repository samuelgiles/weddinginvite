# frozen_string_literal: true

module App
  module Services
    class AuthenticationHelpers
      def initialize(session_manager)
        @session_manager = session_manager
      end

      def current_user
        @current_user ||= @session_manager.current_user.value!
      end

      def current_wedding
        @current_wedding ||= @session_manager.current_wedding.value!
      end

      def logged_in?
        @logged_in ||= !!@session_manager.current_user.value_or(false)
      end
    end
  end
end
