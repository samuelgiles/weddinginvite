# frozen_string_literal: true

require 'wedding_invite/authentication/use_cases/user_attempts_login'
require 'wedding_invite/authentication/interfaces/persistence'
require 'wedding_invite/messaging/interfaces/service_provider'
require 'clean/errors'

module WeddingInvite
  module Authentication
    module UseCases
      describe UserAttemptsLogin do
        let(:use_case) do
          described_class.new(parameters, authentication_persistence, messaging_service_provider)
        end

        let(:parameters) { described_class::Parameters.new(parameters_hash) }
        let(:parameters_hash) do
          { phone_number: '+447543651833' }
        end
        let(:authentication_persistence) { instance_double(Interfaces::Persistence) }
        let(:messaging_service_provider) { instance_double(Messaging::Interfaces::ServiceProvider) }

        describe '#result' do
          subject(:result) { use_case.result }

          let(:validator) do
            instance_double(Validators::UserAttemptsLoginParameters)
          end

          before do
            expect(Validators::UserAttemptsLoginParameters)
              .to receive(:new)
              .with(parameters, authentication_persistence: authentication_persistence)
              .and_return(validator)
            expect(validator)
              .to receive(:result)
              .and_return(validator_result)
          end

          context 'when the parameters are valid' do
            let(:validator_result) { Dry::Monads::Success(parameters) }

            let(:message) { instance_double(Messaging::Entities::Message) }

            before do
              expect(authentication_persistence)
                .to receive(:authentication_code_for)
                .with('+447543651833')
                .and_return('2319')
              expect(Messaging::Entities::Message)
                .to receive(:new)
                .with(
                  phone_number: '+447543651833',
                  content: '💌 Your WeddingInvite.io verification code is 2319'
                )
                .and_return(message)
              expect(messaging_service_provider)
                .to receive(:send_message)
                .with(message)
            end

            it { is_expected.to be_a_success_with_value('+447543651833') }
          end

          context 'when the parameters are not valid' do
            let(:validator_result) { Dry::Monads::Failure(errors) }
            let(:errors) { instance_double(Clean::Errors) }

            it { is_expected.to be_a_failure_with_value(errors) }
          end
        end
      end
    end
  end
end
