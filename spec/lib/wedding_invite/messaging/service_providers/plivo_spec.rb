# frozen_string_literal: true

require 'plivo'
require 'wedding_invite/messaging/service_providers/plivo'
require 'wedding_invite/messaging/entities/message'

module WeddingInvite
  module Messaging
    module ServiceProviders
      describe Plivo do
        let(:service_provider) { described_class.new(plivo_client, source_number) }

        let(:plivo_client) { instance_double(::Plivo::RestClient) }
        let(:source_number) { 'WeddingInvite.io' }

        describe '#send_message' do
          subject(:send_message) { service_provider.send_message(message) }

          let(:message) do
            Entities::Message.new(
              phone_number: '+447543651833',
              content: 'Yo dawg, I heard you like clean architecture'
            )
          end
          let(:plivo_client_messages) { instance_double(::Plivo::Resources::MessagesInterface) }
          let(:plivo_response) { instance_double(::Plivo::Base::Response) }

          before do
            expect(plivo_client)
              .to receive(:messages)
              .and_return(plivo_client_messages)
          end

          specify do
            expect(plivo_client_messages)
              .to receive(:create)
              .with(
                'WeddingInvite.io',
                ['+447543651833'],
                'Yo dawg, I heard you like clean architecture'
              )
              .and_return(plivo_response)
            send_message
          end
        end
      end
    end
  end
end
