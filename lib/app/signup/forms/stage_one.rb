# frozen_string_literal: true

require 'clean/abstract_parameter_object_form'
require 'wedding_invite/signup/use_cases/user_fills_in_stage_one'

module App
  module Signup
    module Forms
      # Form object for the UserFillsInStageOne use case
      class StageOne < Clean::AbstractParameterObjectForm
        acts_as_form_for WeddingInvite::Signup::UseCases::UserFillsInStageOne::Parameters

        private
        
        def parameter_object_hash
          super.merge(
            mobile_phone_number: international_mobile_phone_number,
            partner_mobile_phone_number: international_partner_mobile_phone_number
          )
        end

        def international_mobile_phone_number
          Phonelib.parse(@params[:mobile_phone_number]).international || ''
        end

        def international_partner_mobile_phone_number
          Phonelib.parse(@params[:partner_mobile_phone_number]).international || ''
        end
      end
    end
  end
end
