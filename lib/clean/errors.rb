# frozen_string_literal: true

require 'active_model/errors'

module Clean
  # In the interests of not directly relying on framework..
  class Errors < ActiveModel::Errors
  end
end
