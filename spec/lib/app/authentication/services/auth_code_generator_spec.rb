# frozen_string_literal: true

require 'rails_helper'

module App
  module Authentication
    module Services
      describe AuthCodeGenerator do
        let(:service) { described_class.new(digits, seed) }

        let(:digits) { 6 }
        let(:seed) { 1234 }

        describe '#generate' do
          subject(:generate) { service.generate }

          context 'when asked for 6 digits' do
            let(:digits) { 6 }

            it { is_expected.to eq '365489' }
          end

          context 'when asked for 3 digits' do
            let(:digits) { 3 }

            it { is_expected.to eq '365' }
          end
        end
      end
    end
  end
end
