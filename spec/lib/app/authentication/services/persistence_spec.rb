# frozen_string_literal: true

require 'rails_helper'

module App
  module Authentication
    module Services
      describe Persistence do
        let(:service) { described_class.new }

        it_behaves_like 'it implements', WeddingInvite::Authentication::Interfaces::Persistence

        describe '#phone_number_belongs_to_a_user?' do
          subject { service.phone_number_belongs_to_a_user?(phone_number) }

          let(:phone_number) { '+447543651833' }
          let(:person_relation) { instance_double(ActiveRecord::Relation) }

          before do
            expect(Person)
              .to receive(:where)
              .with(mobile_phone_number: '+447543651833')
              .and_return(person_relation)
            expect(person_relation).to receive(:exists?).and_return(person_with_phone_exists)
          end

          context 'when a person with the phone number exists' do
            let(:person_with_phone_exists) { true }

            it { is_expected.to be true }
          end

          context 'when a person with the phone number does not exist' do
            let(:person_with_phone_exists) { false }

            it { is_expected.to be false }
          end
        end

        describe '#authentication_code_for' do
          subject(:authentication_code) { service.authentication_code_for(phone_number) }

          let(:phone_number) { '+447543651833' }
          let(:auth_code_generator) { instance_double(AuthCodeGenerator) }
          let(:invalidate_active_authentication_codes_command) do
            instance_double(Commands::InvalidateActiveAuthenticationCodes)
          end

          before do
            expect(AuthCodeGenerator).to receive(:new).with(4).and_return(auth_code_generator)
            expect(auth_code_generator).to receive(:generate).and_return('2319')
            expect(Commands::InvalidateActiveAuthenticationCodes)
              .to receive(:new)
              .with('+447543651833')
              .and_return(invalidate_active_authentication_codes_command)
          end

          specify do
            expect(invalidate_active_authentication_codes_command).to receive(:execute)
            expect(AuthenticationCode)
              .to receive(:create!)
              .with(phone_number: '+447543651833', code: '2319')
            expect(authentication_code).to eq '2319'
          end
        end

        describe '#verify_authentication_code' do
          subject(:verify_authentication_code) do
            service.verify_authentication_code(phone_number, authentication_code)
          end

          let(:phone_number) { '+447543651833' }
          let(:authentication_code) { '2319' }
          let(:valid_authentication_code_query) do
            instance_double(Queries::ValidAuthenticationCode)
          end

          before do
            expect(Queries::ValidAuthenticationCode)
              .to receive(:new)
              .with('+447543651833', '2319')
              .and_return(valid_authentication_code_query)
            expect(valid_authentication_code_query)
              .to receive(:exists?)
              .and_return(valid_code_exists)
          end

          context 'when a valid code exists' do
            let(:valid_code_exists) { true }

            it { is_expected.to be_a_success_with_value('+447543651833') }
          end

          context 'when a valid code does not exist' do
            let(:valid_code_exists) { false }

            it { is_expected.to be_a_failure_with_value('+447543651833') }
          end
        end

        describe '#login_token_for' do
          subject(:login_token_for) { service.login_token_for(phone_number, friendly_identifier) }

          let(:phone_number) { '+447543651833' }
          let(:friendly_identifier) { 'Web App' }
          let(:person) { instance_double(Person) }
          let(:login_sessions_relation) { instance_double(ActiveRecord::Relation) }
          let(:login_session) { instance_double(LoginSession) }

          before do
            expect(SecureRandom)
              .to receive(:uuid)
              .and_return('35d9e183-c920-4814-b072-a41e529bba50')
            expect(Person)
              .to receive(:find_by!)
              .with(mobile_phone_number: '+447543651833')
              .and_return(person)
            expect(person).to receive(:login_sessions).and_return(login_sessions_relation)
          end

          specify do
            expect(login_sessions_relation)
              .to receive(:create!)
              .with(
                token: '35d9e183-c920-4814-b072-a41e529bba50',
                friendly_identifier: 'Web App'
              )
              .and_return(login_session)
            expect(login_token_for).to eq '35d9e183-c920-4814-b072-a41e529bba50'
          end
        end
      end
    end
  end
end
