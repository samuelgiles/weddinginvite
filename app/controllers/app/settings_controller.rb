# frozen_string_literal: true

require "dry/matcher/result_matcher"

module App
  class SettingsController < BaseController
    requires_user_to_be_logged_in
    uses_navigation_screen

    def index
    end
  end
end
