# frozen_string_literal: true

require "dry/matcher/result_matcher"
require 'wedding_invite/authentication/use_cases/user_attempts_login'

module App
  class SessionsController < BaseController
    def new
      @form = Authentication::Forms::Login.new
    end

    def create
      Dry::Matcher::ResultMatcher.call(user_attempts_login_result) do |matcher|
        matcher.success do |phone_number|
          session[:verifying_login_for_phone_number] = phone_number
          redirect_to new_app_verification_path
        end

        matcher.failure do |errors|
          @form = login_form.with_errors(errors)
          render :new
        end
      end
    end

    private

    PERMITTED_CREATE_ATTRIBUTES = %i[phone_number]

    def create_params
      params.require(:app_authentication_forms_login).permit(*PERMITTED_CREATE_ATTRIBUTES)
    end

    def login_form
      @login_form ||= Authentication::Forms::Login.new(params: create_params.to_h)
    end

    def user_attempts_login_parameters
      login_form.to_parameter_object
    end

    def user_attempts_login_result
      WeddingInvite::Authentication::UseCases::UserAttemptsLogin.new(
        user_attempts_login_parameters,
        authentication_persistence,
        messaging_service_provider
      ).result
    end
  end
end
