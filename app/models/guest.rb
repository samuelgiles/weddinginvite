# frozen_string_literal: true

class Guest < ApplicationRecord
  belongs_to :invite

  validates :invite, presence: true
end
