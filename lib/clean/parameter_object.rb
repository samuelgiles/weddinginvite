# frozen_string_literal: true

require 'dry/struct'

module Clean
  class ParameterObject < Dry::Struct::Value
  end
end
