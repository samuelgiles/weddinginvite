# frozen_string_literal: true

require 'app/types'
require 'dry/struct'

module App
  module Signup
    module Entities
      class Settings < Dry::Struct::Value
        attribute :base_domain, Types::Strict::String
      end
    end
  end
end
