# frozen_string_literal: true

require 'wedding_invite/types'
require 'wedding_invite/signup/entities/stage_two'
require 'wedding_invite/signup/validators/user_fills_in_stage_two_parameters'
require 'clean/abstract_use_case'

module WeddingInvite
  module Signup
    module UseCases
      class UserFillsInStageTwo < Clean::AbstractUseCase
        class Parameters < Clean::ParameterObject
          attribute :name, Types::Strict::String.meta(omittable: true)
          attribute :domain, Types::Strict::String.meta(omittable: true)
        end

        def initialize(parameters, signup_persistence)
          @parameters = parameters
          @signup_persistence = signup_persistence
        end

        def result
          validator.result.bind do |_|
            @signup_persistence.persist_stage_two(stage_two)
            Dry::Monads::Success(stage_two)
          end
        end

        private

        def stage_two
          @stage_two ||= Entities::StageTwo.new(@parameters)
        end

        def validator
          @validator ||= Validators::UserFillsInStageTwoParameters.new(
            @parameters,
            signup_persistence: @signup_persistence
          )
        end
      end
    end
  end
end
