# frozen_string_literal: true

module App
  module Authentication
    module Commands
      class InvalidateActiveAuthenticationCodes
        def initialize(phone_number)
          @phone_number = phone_number
        end

        def execute
          AuthenticationCode
            .active
            .where(phone_number: @phone_number)
            .update_all(expired: true)
        end
      end
    end
  end
end
