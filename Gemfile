# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.0'

# Cleaaaan
gem 'dry-monads'
gem 'dry-matcher'
gem 'dry-struct'
gem 'dry-types'
gem 'duckface-interfaces'

# Framework:
gem 'bcrypt', '~> 3.1.7'
gem 'bootsnap', require: false
gem 'foreman', require: false
gem 'jbuilder', '~> 2.5'
gem 'mini_magick', '~> 4.8'
gem 'puma', '~> 3.11'
gem 'rails', '~> 5.2.1'
gem 'turbolinks', '~> 5', require: false
gem 'webpacker'

gem 'pg'

# Application:
gem 'phonelib'
gem 'plivo', '>= 4.1.4', require: false
gem 'inline_svg'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'parallel_tests'
  gem 'rspec'
  gem 'rspec-rails', '~> 3.7'
  gem 'factory_bot'
  gem 'rails-controller-testing'
  gem 'timecop'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'reek'
  gem 'rubocop'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'capybara', '>= 2.15'
  gem 'chromedriver-helper'
  gem 'selenium-webdriver'
  gem 'simplecov', require: false
end

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
