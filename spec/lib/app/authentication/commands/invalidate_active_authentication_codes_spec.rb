# frozen_string_literal: true

require 'rails_helper'

module App
  module Authentication
    module Commands
      describe InvalidateActiveAuthenticationCodes do
        let(:command) { described_class.new('+447543651833') }

        describe '#execute' do
          subject(:execute) { command.execute }

          let(:authentication_code_relation) { instance_double(ActiveRecord::Relation) }

          before do
            expect(AuthenticationCode).to receive(:active).and_return(authentication_code_relation)
            expect(authentication_code_relation)
              .to receive(:where)
              .with(phone_number: '+447543651833')
              .and_return(authentication_code_relation)
          end

          specify do
            expect(authentication_code_relation)
              .to receive(:update_all)
              .with(expired: true)
            execute
          end
        end
      end
    end
  end
end
