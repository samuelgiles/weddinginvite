# frozen_string_literal: true

require 'rails_helper'

module App
  describe SetupController do
    describe '#show' do
      subject(:make_request) { get :show }

      it_behaves_like 'a controller action that requires users to be logged in'

      context 'when logged in' do
        let(:current_wedding) { instance_double(WeddingInvite::Entities::Wedding) }
        let(:setup_dashboard_presenter) { instance_double(Presenters::SetupDashboard) }

        before do
          expect(controller).to receive(:logged_in?).and_return(true)
          expect(controller).to receive(:current_wedding).and_return(current_wedding)
          expect(controller).to receive(:prepare_navigation)
          expect(Presenters::SetupDashboard)
            .to receive(:new)
            .with(current_wedding)
            .and_return(setup_dashboard_presenter)
        end

        specify do
          make_request
          expect(assigns[:setup_dashboard]).to eq setup_dashboard_presenter
        end
      end
    end
  end
end
