# frozen_string_literal: true

require 'rails_helper'

module App
  describe SessionsController do
    describe '#new' do
      subject(:make_request) { get :new }

      let(:login_form) { instance_double(Authentication::Forms::Login) }

      before do
        expect(Authentication::Forms::Login)
          .to receive(:new)
          .and_return(login_form)
      end

      specify do
        make_request
        expect(assigns[:form]).to eq login_form
      end
    end

    describe '#create' do
      subject(:make_request) { post :create, params: params }

      let(:params) do
        {
          app_authentication_forms_login: permitted_params
        }
      end
      let(:permitted_params) do
        {
          phone_number: '07543651833'
        }
      end
      let(:login_form) { instance_double(Authentication::Forms::Login) }
      let(:user_attempts_login_parameters) do
        instance_double(WeddingInvite::Authentication::UseCases::UserAttemptsLogin::Parameters)
      end
      let(:authentication_persistence) { instance_double(Authentication::Services::Persistence) }
      let(:messaging_service_provider) do
        instance_double(WeddingInvite::Messaging::Interfaces::ServiceProvider)
      end
      let(:user_attempts_login_use_case) do
        instance_double(WeddingInvite::Authentication::UseCases::UserAttemptsLogin)
      end

      before do
        expect(Authentication::Forms::Login)
          .to receive(:new)
          .with(params: { phone_number: '07543651833' })
          .and_return(login_form)
        expect(login_form)
          .to receive(:to_parameter_object)
          .and_return(user_attempts_login_parameters)
        expect(controller)
          .to receive(:authentication_persistence)
          .and_return(authentication_persistence)
        expect(controller)
          .to receive(:messaging_service_provider)
          .and_return(messaging_service_provider)
        expect(WeddingInvite::Authentication::UseCases::UserAttemptsLogin)
          .to receive(:new)
          .with(
            user_attempts_login_parameters,
            authentication_persistence,
            messaging_service_provider
          )
          .and_return(user_attempts_login_use_case)
        expect(user_attempts_login_use_case)
          .to receive(:result)
          .and_return(user_attempts_login_result)
      end

      context 'when use case result is successful' do
        let(:user_attempts_login_result) { Dry::Monads::Success('+44 7543 651833') }

        specify do
          make_request
          expect(session[:verifying_login_for_phone_number]).to eq '+44 7543 651833'
          expect(response).to redirect_to(new_app_verification_path)
        end
      end

      context 'when use case result is unsuccessful' do
        let(:user_attempts_login_result) { Dry::Monads::Failure(errors) }
        let(:errors) { instance_double(Clean::Errors) }
        let(:login_form_with_errors) { instance_double(Authentication::Forms::Login) }

        before do
          expect(login_form)
            .to receive(:with_errors)
            .with(errors)
            .and_return(login_form_with_errors)
        end

        specify do
          make_request
          expect(assigns[:form]).to eq login_form_with_errors
          expect(response).to render_template(:new)
        end
      end
    end
  end
end
