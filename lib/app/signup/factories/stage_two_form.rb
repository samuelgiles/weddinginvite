# frozen_string_literal: true

require 'app/signup/forms/stage_two'
require 'dry/matcher/result_matcher'
require 'wedding_invite/signup/factories/prepopulated_stage_two'

module App
  module Signup
    module Factories
      class StageTwoForm
        def initialize(signup_persistence)
          @signup_persistence = signup_persistence
        end

        def manufacture
          Forms::StageTwo.new(
            params: StageTwoParamsSerializer.new(stage_two).to_h,
            context: { base_domain: @signup_persistence.base_domain }
          )
        end

        private

        def stage_two
          Dry::Matcher::ResultMatcher.call(load_stage_two_result) do |matcher|
            matcher.success do |existing_stage_two|
              existing_stage_two
            end

            matcher.failure do |_|
              WeddingInvite::Signup::Factories::PrepopulatedStageTwo.new(stage_one).manufacture
            end
          end
        end

        # We'll raise an exception if someone is filling out stage two when they
        # don't yet have a stage one as its something that shouldn't ever happen:
        def stage_one
          load_stage_one_result.value!
        end

        def load_stage_one_result
          @load_stage_one_result ||= @signup_persistence.load_stage_one
        end

        def load_stage_two_result
          @load_stage_two_result ||= @signup_persistence.load_stage_two
        end

        class StageTwoParamsSerializer
          def initialize(stage_two)
            @stage_two = stage_two
          end

          def to_h
            @stage_two.attributes
          end
        end
      end
    end
  end
end
