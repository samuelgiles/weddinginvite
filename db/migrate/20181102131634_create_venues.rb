# frozen_string_literal: true

class CreateVenues < ActiveRecord::Migration[5.2]
  def change
    create_table :venues do |t|
      t.string :name, null: false
      t.string :address_line_1, null: false
      t.string :address_line_2
      t.string :city, null: false
      t.string :county, null: false
      t.string :postcode, null: false
      t.string :country, null: false
      t.string :latitude
      t.string :longitude

      t.timestamps
    end
  end
end
