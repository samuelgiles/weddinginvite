# frozen_string_literal: true

module App
  class AppController < BaseController
    def index
      redirect_to app_setup_path if logged_in?
    end
  end
end
