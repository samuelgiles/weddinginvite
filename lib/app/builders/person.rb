# frozen_string_literal: true

module App
  module Builders
    class Person < AbstractActiveRecordEntityBuilder
      acts_as_builder_for_entity WeddingInvite::Entities::Person
    end
  end
end
