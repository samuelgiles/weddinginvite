# frozen_string_literal: true

class CreateLoginSessions < ActiveRecord::Migration[5.2]
  def change
    create_table :login_sessions do |t|
      t.string :token, null: false
      t.references :person, foreign_key: true
      t.datetime :last_seen
      t.string :friendly_identifier, null: false

      t.timestamps
    end
    add_index :login_sessions, :token, unique: true
  end
end
