# frozen_string_literal: true

class Venue < ApplicationRecord
  has_many :events, dependent: :nullify

  validates :name,
            :address_line_1,
            :city,
            :county,
            :postcode,
            :country,
            presence: true
end
