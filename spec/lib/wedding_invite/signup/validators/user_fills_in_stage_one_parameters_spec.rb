# frozen_string_literal: true

require 'wedding_invite/signup/validators/user_fills_in_stage_one_parameters'
require 'wedding_invite/signup/use_cases/user_fills_in_stage_one'
require 'wedding_invite/signup/interfaces/persistence'

module WeddingInvite
  module Signup
    module Validators
      describe UserFillsInStageOneParameters do
        let(:validator) do
          described_class.new(
            user_fills_in_stage_one_parameters,
            signup_persistence: signup_persistence
          )
        end
        let(:user_fills_in_stage_one_parameters) do
          instance_double(
            UseCases::UserFillsInStageOne::Parameters,
            forename: 'Samuel',
            surname: 'Giles',
            partner_forename: 'Stephanie',
            partner_surname: 'Collins',
            mobile_phone_number: mobile_phone_number,
            partner_mobile_phone_number: partner_mobile_phone_number
          )
        end
        let(:mobile_phone_number) { '+447543651833' }
        let(:partner_mobile_phone_number) { nil }
        let(:signup_persistence) { instance_double(Interfaces::Persistence) }

        specify do
          expect(described_class).to have_validator(
            ActiveModel::Validations::PresenceValidator,
            %i[forename surname mobile_phone_number partner_forename partner_surname]
          )
          expect(described_class).to have_validator(
            PhoneValidator,
            %i[mobile_phone_number partner_mobile_phone_number]
          )
        end

        describe '#ensure_mobile_phone_number_is_available' do
          subject(:result) { validator.result }

          before do
            expect(signup_persistence)
              .to receive(:mobile_phone_number_is_available?)
              .with('+447543651833')
              .and_return(mobile_phone_number_is_available)
          end

          context 'when the phone number is available' do
            let(:mobile_phone_number_is_available) { true }

            it { is_expected.to be_a_success_with_value(user_fills_in_stage_one_parameters) }
          end

          context 'when the phone number is not available' do
            let(:mobile_phone_number_is_available) { false }

            specify do
              expect(result.failure.full_messages).to contain_exactly(
                "Mobile phone number is already in use"
              )
            end
          end
        end

        describe '#ensure_partner_mobile_phone_number_is_available' do
          subject(:result) { validator.result }

          let(:partner_mobile_phone_number) { '+447807601204' }

          before do
            expect(signup_persistence)
              .to receive(:mobile_phone_number_is_available?)
              .with('+447543651833')
              .and_return(true)
            expect(signup_persistence)
              .to receive(:mobile_phone_number_is_available?)
              .with('+447807601204')
              .and_return(mobile_phone_number_is_available)
          end

          context 'when the phone number is available' do
            let(:mobile_phone_number_is_available) { true }

            it { is_expected.to be_a_success_with_value(user_fills_in_stage_one_parameters) }
          end

          context 'when the phone number is not available' do
            let(:mobile_phone_number_is_available) { false }

            specify do
              expect(result.failure.full_messages).to contain_exactly(
                "Partner mobile phone number is already in use"
              )
            end
          end
        end

        describe '#ensure_mobile_phone_numbers_are_unique' do
          subject(:result) { validator.result }

          before do
            expect(signup_persistence)
              .to receive(:mobile_phone_number_is_available?)
              .with('+447543651833')
              .and_return(true)
              .at_most(2)
              .times
          end

          context 'when the partner mobile phone number is not present' do
            let(:partner_mobile_phone_number) { nil }

            it { is_expected.to be_a_success_with_value(user_fills_in_stage_one_parameters) }
          end

          context 'when the partner mobile phone number is present' do
            context 'when the numbers are the same' do
              let(:partner_mobile_phone_number) { '+447543651833' }

              specify do
                expect(result.failure.full_messages).to contain_exactly(
                  "Partner mobile phone number cannot be the same as your mobile phone number"
                )
              end
            end

            context 'when the numbers are different' do
              let(:partner_mobile_phone_number) { '+447807601204' }

              before do
                expect(signup_persistence)
                  .to receive(:mobile_phone_number_is_available?)
                  .with('+447807601204')
                  .and_return(true)
              end

              it { is_expected.to be_a_success_with_value(user_fills_in_stage_one_parameters) }
            end
          end
        end
      end
    end
  end
end
