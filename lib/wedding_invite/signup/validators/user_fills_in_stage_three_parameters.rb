# frozen_string_literal: true

require 'clean/abstract_validator'

module WeddingInvite
  module Signup
    module Validators
      class UserFillsInStageThreeParameters < Clean::AbstractValidator
        performs_validations_on :date,
                                :terms_agreed

        validates :date, presence: true
        validate :date_must_be_in_future,
                 :terms_must_be_agreed

        private

        def date_must_be_in_future
          return unless date
          return if DateTime.now < date
          errors.add(:date, 'cannot be in the past')
        end

        def terms_must_be_agreed
          return true if terms_agreed
          errors.add(:terms_agreed, "must be checked")
        end
      end
    end
  end
end
