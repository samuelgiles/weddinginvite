# frozen_string_literal: true

class CreateGuests < ActiveRecord::Migration[5.2]
  def change
    create_table :guests do |t|
      t.string :forename, null: false
      t.string :surname, null: false
      t.integer :status, null: false, default: 0
      t.datetime :status_updated_at
      t.references :invite, foreign_key: true

      t.timestamps
    end
  end
end
