# frozen_string_literal: true

require 'app/factories/plivo_messaging_service_provider'

module App
  module Factories
    describe PlivoMessagingServiceProvider do
      let(:factory) { described_class.new(credentials) }

      let(:credentials) do
        {
          plivo: {
            auth_id: 'secret',
            auth_token: 'another-secret',
            source_number: '+447543651833'
          }
        }
      end

      describe '#manufacture' do
        subject(:manufacture) { factory.manufacture }

        let(:plivo_client_factory) do
          instance_double(PlivoClient)
        end
        let(:plivo_client) { instance_double(::Plivo::RestClient) }
        let(:plivo_messaging_service_provider) do
          instance_double(WeddingInvite::Messaging::ServiceProviders::Plivo)
        end

        before do
          expect(PlivoClient).to receive(:new).with(credentials).and_return(plivo_client_factory)
          expect(plivo_client_factory).to receive(:manufacture).and_return(plivo_client)
          expect(WeddingInvite::Messaging::ServiceProviders::Plivo)
            .to receive(:new)
            .with(plivo_client, '+447543651833')
            .and_return(plivo_messaging_service_provider)
        end

        it { is_expected.to eq plivo_messaging_service_provider }
      end
    end
  end
end
