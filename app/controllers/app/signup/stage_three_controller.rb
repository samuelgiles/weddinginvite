# frozen_string_literal: true

require 'app/signup/factories/stage_two_form'

module App
  module Signup
    class StageThreeController < AbstractSignupController
      def new
        @form = Forms::StageThree.new
      end

      def create
        Dry::Matcher::ResultMatcher.call(user_fills_in_stage_three_result) do |matcher|
          matcher.success do |_stage_three|
            redirect_to app_signup_complete_path
          end

          matcher.failure do |errors|
            @form = populated_stage_three_form.with_errors(errors)
            render :new
          end
        end
      end

      private

      PERMITTED_CREATE_ATTRIBUTES = %i[
        date
        terms_agreed
      ]

      def create_params
        params.require(:app_signup_forms_stage_three).permit(*PERMITTED_CREATE_ATTRIBUTES)
      end

      def user_fills_in_stage_three_parameters
        populated_stage_three_form.to_parameter_object
      end

      def populated_stage_three_form
        @populated_stage_three_form ||= Forms::StageThree.new(params: create_params.to_h)
      end

      def user_fills_in_stage_three_result
        WeddingInvite::Signup::UseCases::UserFillsInStageThree.new(
          user_fills_in_stage_three_parameters,
          signup_persistence
        ).result
      end
    end
  end
end
