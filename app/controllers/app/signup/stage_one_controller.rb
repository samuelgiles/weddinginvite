# frozen_string_literal: true

require 'app/signup/factories/stage_one_form'
require 'app/signup/forms/stage_one'
require 'wedding_invite/signup/use_cases/user_fills_in_stage_one'
require "dry/matcher/result_matcher"

module App
  module Signup
    class StageOneController < AbstractSignupController
      def new
        @form = Factories::StageOneForm.new(signup_persistence).manufacture
      end

      def create
        Dry::Matcher::ResultMatcher.call(user_fills_in_stage_one_result) do |matcher|
          matcher.success do |_stage_one|
            redirect_to new_app_signup_stage_two_path
          end

          matcher.failure do |errors|
            @form = populated_stage_one_form.with_errors(errors)
            render :new
          end
        end
      end

      private

      PERMITTED_CREATE_ATTRIBUTES = %i[
        forename
        surname
        mobile_phone_number
        partner_forename
        partner_surname
        partner_mobile_phone_number
      ]

      def create_params
        params.require(:app_signup_forms_stage_one).permit(*PERMITTED_CREATE_ATTRIBUTES)
      end

      def populated_stage_one_form
        @populated_stage_one_form ||= Forms::StageOne.new(params: create_params.to_h)
      end

      def user_fills_in_stage_one_parameters
        populated_stage_one_form.to_parameter_object
      end

      def user_fills_in_stage_one_result
        WeddingInvite::Signup::UseCases::UserFillsInStageOne.new(
          user_fills_in_stage_one_parameters,
          signup_persistence
        ).result
      end
    end
  end
end
