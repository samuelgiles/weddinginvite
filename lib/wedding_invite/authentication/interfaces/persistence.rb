# frozen_string_literal: true

require 'duckface'

module WeddingInvite
  module Authentication
    module Interfaces
      module Persistence
        extend Duckface::ActsAsInterface

        def phone_number_belongs_to_a_user?(_phone_number)
          raise NotImplementedError
        end

        def authentication_code_for(_phone_number)
          raise NotImplementedError
        end

        def verify_authentication_code(_phone_number, _authentication_code)
          raise NotImplementedError
        end

        def login_token_for(_phone_number, _friendly_identifier)
          raise NotImplementedError
        end
      end
    end
  end
end
