# frozen_string_literal: true

class Event < ApplicationRecord
  belongs_to :venue, optional: true

  validates :starts_at, :details, presence: true
end
