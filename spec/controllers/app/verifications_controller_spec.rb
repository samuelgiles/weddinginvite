# frozen_string_literal: true

require 'rails_helper'

module App
  describe VerificationsController do
    describe '#new' do
      subject(:make_request) { get :new }

      let(:verify_form) { instance_double(Authentication::Forms::Login) }

      before do
        session[:verifying_login_for_phone_number] = '+447543651833'
        expect(Authentication::Forms::Verify)
          .to receive(:new)
          .with(params: { phone_number: '+447543651833' })
          .and_return(verify_form)
      end

      specify do
        make_request
        expect(assigns[:form]).to eq verify_form
      end
    end

    describe '#create' do
      subject(:make_request) { post :create, params: params }

      let(:params) do
        {
          app_authentication_forms_verify: permitted_params
        }
      end
      let(:permitted_params) do
        {
          phone_number: '+447543651833',
          authentication_code: '2319'
        }
      end
      let(:verify_form) { instance_double(Authentication::Forms::Verify) }
      let(:user_verifies_login_parameters) do
        instance_double(WeddingInvite::Authentication::UseCases::UserVerifiesLogin::Parameters)
      end
      let(:authentication_persistence) { instance_double(Authentication::Services::Persistence) }
      let(:user_verifies_login_use_case) do
        instance_double(WeddingInvite::Authentication::UseCases::UserVerifiesLogin)
      end

      before do
        session[:verifying_login_for_phone_number] = '+447543651833'
        expect(Authentication::Forms::Verify)
          .to receive(:new)
          .with(params: { phone_number: '+447543651833', authentication_code: '2319' })
          .and_return(verify_form)
        expect(verify_form)
          .to receive(:to_parameter_object)
          .and_return(user_verifies_login_parameters)
        expect(controller)
          .to receive(:authentication_persistence)
          .and_return(authentication_persistence)
        expect(WeddingInvite::Authentication::UseCases::UserVerifiesLogin)
          .to receive(:new)
          .with(
            user_verifies_login_parameters,
            authentication_persistence
          )
          .and_return(user_verifies_login_use_case)
        expect(user_verifies_login_use_case)
          .to receive(:result)
          .and_return(user_verifies_login_use_case_result)
      end

      context 'when use case result is successful' do
        let(:user_verifies_login_use_case_result) { Dry::Monads::Success('secret-login-token') }
        let(:session_manager) do
          instance_double(WeddingInvite::Authentication::Interfaces::SessionManager)
        end

        before do
          expect(controller).to receive(:session_manager).and_return(session_manager)
        end

        specify do
          expect(session[:verifying_login_for_phone_number]).not_to be_nil
          expect(session_manager).to receive(:use_login_token).with('secret-login-token')
          make_request
          expect(session[:verifying_login_for_phone_number]).to be_nil
          expect(response).to redirect_to(app_root_path)
        end
      end

      context 'when use case result is unsuccessful' do
        let(:user_verifies_login_use_case_result) { Dry::Monads::Failure(false) }
        let(:verify_form_with_error_message) { instance_double(Authentication::Forms::Verify) }

        before do
          expect(verify_form)
            .to receive(:with_error_message)
            .with(
              "That code doesn't look quite right, re-enter the code or try again.",
              override_params: { phone_number: '+447543651833' }
            )
            .and_return(verify_form_with_error_message)
        end

        specify do
          make_request
          expect(assigns[:form]).to eq verify_form_with_error_message
          expect(response).to render_template(:new)
        end
      end
    end
  end
end
