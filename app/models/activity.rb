# frozen_string_literal: true

class Activity < ApplicationRecord
  belongs_to :subject, polymorphic: true
  belongs_to :seen_by, optional: true

  validates :subject, presence: true
end
