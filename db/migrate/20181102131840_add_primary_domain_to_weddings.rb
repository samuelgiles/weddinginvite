# frozen_string_literal: true

class AddPrimaryDomainToWeddings < ActiveRecord::Migration[5.2]
  def change
    add_reference :weddings, :primary_domain, foreign_key: { to_table: :domains }
  end
end
