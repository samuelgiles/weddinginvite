# frozen_string_literal: true

require 'wedding_invite/types'
require 'dry/struct'

module WeddingInvite
  module Signup
    module Entities
      class CompleteSignup < Dry::Struct::Value
        attribute :partner_forename, Types::Strict::String
        attribute :login_token, Types::Strict::String
      end
    end
  end
end
