# frozen_string_literal: true

require 'wedding_invite/types'
require 'wedding_invite/signup/entities/stage_three'
require 'wedding_invite/signup/validators/user_fills_in_stage_three_parameters'
require 'clean/abstract_use_case'

module WeddingInvite
  module Signup
    module UseCases
      class UserFillsInStageThree < Clean::AbstractUseCase
        class Parameters < Clean::ParameterObject
          attribute :date, Types::Params::Date.meta(omittable: true)
          attribute :terms_agreed, Types::Params::Bool.meta(omittable: true).default(false)
        end

        def initialize(parameters, signup_persistence)
          @parameters = parameters
          @signup_persistence = signup_persistence
        end

        def result
          validator.result.bind do |_|
            @signup_persistence.persist_signup(stage_one, stage_two, stage_three)
            # TODO: Send them a text with login details
            Dry::Monads::Success(stage_three)
          end
        end

        private

        def stage_one
          @signup_persistence.load_stage_one.value!
        end

        def stage_two
          @signup_persistence.load_stage_two.value!
        end

        def stage_three
          @stage_three ||= Entities::StageThree.new(@parameters)
        end

        def validator
          @validator ||= Validators::UserFillsInStageThreeParameters.new(@parameters)
        end
      end
    end
  end
end
