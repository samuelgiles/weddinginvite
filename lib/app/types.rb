# frozen_string_literal: true

require 'dry-types'

module App
  module Types
    include Dry::Types.module
  end
end
