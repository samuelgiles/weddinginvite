# frozen_string_literal: true

require 'dry/monads/result'
require 'dry/monads/try'
require 'wedding_invite/authentication/interfaces/persistence'
require 'app/authentication/commands/invalidate_active_authentication_codes'

module App
  module Authentication
    module Services
      class Persistence
        implements_interface WeddingInvite::Authentication::Interfaces::Persistence

        def phone_number_belongs_to_a_user?(phone_number)
          Person.where(mobile_phone_number: phone_number).exists?
        end

        AUTH_CODE_DIGITS = 4

        def authentication_code_for(phone_number)
          Commands::InvalidateActiveAuthenticationCodes.new(phone_number).execute
          code = AuthCodeGenerator.new(AUTH_CODE_DIGITS).generate
          AuthenticationCode.create!(
            phone_number: phone_number,
            code: code
          )
          code
        end

        def verify_authentication_code(phone_number, authentication_code)
          exists = Queries::ValidAuthenticationCode.new(phone_number, authentication_code).exists?

          return Dry::Monads::Success(phone_number) if exists

          Dry::Monads::Failure(phone_number)
        end

        def login_token_for(phone_number, friendly_identifier)
          login_token = SecureRandom.uuid
          person = Person.find_by!(mobile_phone_number: phone_number)
          person.login_sessions.create!(
            token: login_token,
            friendly_identifier: friendly_identifier
          )
          login_token
        end
      end
    end
  end
end
