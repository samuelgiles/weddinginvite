# frozen_string_literal: true

require 'rails_helper'

module App
  describe StaticContentController do
    describe '#about' do
      subject(:make_request) { get :about }

      specify do
        make_request
        expect(response).to be_successful
      end
    end
  end
end
