# frozen_string_literal: true

require 'clean/abstract_parameter_object_form'
require 'wedding_invite/authentication/use_cases/user_verifies_login'

module App
  module Authentication
    module Forms
      class Verify < Clean::AbstractParameterObjectForm
        acts_as_form_for WeddingInvite::Authentication::UseCases::UserVerifiesLogin::Parameters
      end
    end
  end
end
