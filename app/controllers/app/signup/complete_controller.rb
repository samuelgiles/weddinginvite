# frozen_string_literal: true

module App
  module Signup
    class CompleteController < AbstractSignupController
      def show
        @complete_signup = signup_persistence.load_complete_signup
      end
    end
  end
end
