class CreateWeddings < ActiveRecord::Migration[5.2]
  def change
    create_table :weddings do |t|
      t.string :name, null: false
      t.date :date, null: false
      t.string :theme_identifier
      t.timestamps
    end
  end
end
