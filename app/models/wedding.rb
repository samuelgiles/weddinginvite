# frozen_string_literal: true

class Wedding < ApplicationRecord
  belongs_to :primary_domain, class_name: Domain.name
  has_one :couple, dependent: :destroy
  has_many :people, through: :couple
  has_many :domains, dependent: :destroy

  validates :name, :date, :primary_domain, :couple, presence: true
end
