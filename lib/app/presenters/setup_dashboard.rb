# frozen_string_literal: true

module App
  module Presenters
    class SetupDashboard
      def initialize(wedding)
        @wedding = wedding
      end

      def wedding_name
        @wedding.name
      end

      def formatted_wedding_date
        wedding_date.strftime("%A, #{wedding_date.day.ordinalize} %B %Y")
      end

      def days_until_wedding
        (Date.today..@wedding.date).count
      end

      def steps_completed
        steps.count(true)
      end

      def number_of_steps
        steps.count
      end

      # TODO: Implement
      def picture_uploaded?
        true
      end

      # TODO: Implement
      def venue_details_provided?
        false
      end

      # TODO: Implement
      def theme_chosen?
        true
      end

      private

      def steps
        [picture_uploaded?, venue_details_provided?, theme_chosen?]
      end

      def wedding_date
        @wedding_date ||= @wedding.date
      end
    end
  end
end
