# frozen_string_literal: true

require 'rails_helper'

module App
  module Services
    describe PerRequestRegistry do
      let(:service) { described_class.new(cookies) }

      let(:cookies) { instance_double(ActionDispatch::Cookies::CookieJar) }

      describe '#authentication_persistence' do
        subject { service.authentication_persistence }

        let(:authentication_persistence) { instance_double(Authentication::Services::Persistence) }

        before do
          expect(Authentication::Services::Persistence)
            .to receive(:new)
            .and_return(authentication_persistence)
        end

        it { is_expected.to eq authentication_persistence }
      end

      describe '#messaging_service_provider' do
        subject { service.messaging_service_provider }

        let(:plivo_messaging_service_provider_factory) do
          instance_double(Factories::PlivoMessagingServiceProvider)
        end
        let(:plivo_messaging_service_provider) do
          instance_double(WeddingInvite::Messaging::ServiceProviders::Plivo)
        end

        before do
          expect(Factories::PlivoMessagingServiceProvider)
            .to receive(:new)
            .and_return(plivo_messaging_service_provider_factory)
          expect(plivo_messaging_service_provider_factory)
            .to receive(:manufacture)
            .and_return(plivo_messaging_service_provider)
        end

        it { is_expected.to eq plivo_messaging_service_provider }
      end

      describe '#session_manager' do
        subject { service.session_manager }

        let(:cookie_based_session_manager) do
          instance_double(Authentication::Services::CookieBasedSessionManager)
        end

        before do
          expect(Authentication::Services::CookieBasedSessionManager)
            .to receive(:new)
            .with(cookies)
            .and_return(cookie_based_session_manager)
        end

        it { is_expected.to eq cookie_based_session_manager }
      end
    end
  end
end
