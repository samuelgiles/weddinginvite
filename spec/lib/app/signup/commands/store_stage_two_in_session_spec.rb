# frozen_string_literal: true

require 'app/signup/commands/store_stage_two_in_session'

module App
  module Signup
    module Commands
      describe StoreStageTwoInSession do
        let(:command) { described_class.new(session, stage_two_entity) }
        let(:session) { Hash.new }
        let(:stage_two_entity) { FactoryBot.build(:wedding_invite_signup_stage_two) }

        describe '#execute' do
          subject(:execute) { command.execute }

          specify do
            execute
            expect(session[:signup_stage_two]).to eq(
              name: 'Giles Wedding',
              domain: 'giles-wedding'
            )
          end
        end
      end
    end
  end
end
