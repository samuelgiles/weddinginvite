# frozen_string_literal: true

class Couple < ApplicationRecord
  belongs_to :person_a, class_name: Person.name, dependent: :destroy
  belongs_to :person_b, class_name: Person.name, dependent: :destroy
  belongs_to :wedding

  has_one_attached :picture

  validates :person_a, :person_b, :wedding, presence: true
end
