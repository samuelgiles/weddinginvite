# frozen_string_literal: true

require 'dry/monads/result'
require 'dry/monads/maybe'
require 'wedding_invite/signup/interfaces/persistence'
require 'wedding_invite/signup/entities/stage_one'
require 'wedding_invite/signup/entities/complete_signup'
require 'app/signup/commands/store_stage_one_in_session'
require 'app/signup/commands/store_stage_two_in_session'

module App
  module Signup
    module Services
      # Implements the signup persistence using the session and ActiveRecord
      class Persistence
        implements_interface WeddingInvite::Signup::Interfaces::Persistence

        def initialize(session, settings)
          @session = session
          @settings = settings
        end

        def load_stage_one
          stage_one_hash = @session[:signup_stage_one]
          Dry::Monads::Maybe(stage_one_hash).bind do |hash|
            stage_one = WeddingInvite::Signup::Entities::StageOne.new(hash.symbolize_keys)
            return Dry::Monads::Success(stage_one)
          end
          Dry::Monads::Failure(:no_signup_data)
        end

        def persist_stage_one(stage_one)
          Commands::StoreStageOneInSession.new(@session, stage_one).execute
          stage_one
        end

        def load_stage_two
          stage_two_hash = @session[:signup_stage_two]
          Dry::Monads::Maybe(stage_two_hash).bind do |hash|
            stage_two = WeddingInvite::Signup::Entities::StageTwo.new(hash.symbolize_keys)
            return Dry::Monads::Success(stage_two)
          end
          Dry::Monads::Failure(:no_signup_data)
        end

        def persist_stage_two(stage_two)
          Commands::StoreStageTwoInSession.new(@session, stage_two).execute
          stage_two
        end

        def domain_is_available?(domain)
          complete_domain = [domain, @settings.base_domain].join('.')
          !Domain.where(hostname: complete_domain).exists?
        end

        def base_domain
          @settings.base_domain
        end

        def persist_signup(stage_one, stage_two, stage_three)
          wedding = Factories::Wedding.new(stage_one, stage_two, stage_three, @settings).manufacture
          wedding.save!
          @session[:completed_signup_id] = wedding.id
        end

        def load_complete_signup
          wedding = Wedding.find(@session.fetch(:completed_signup_id))
          WeddingInvite::Signup::Entities::CompleteSignup.new(
            partner_forename: wedding.couple.person_b.forename,
            login_token: 'hello world'
          )
        end

        def mobile_phone_number_is_available?(mobile_phone_number)
          exists = ::Person.where(mobile_phone_number: mobile_phone_number).exists?
          !exists
        end
      end
    end
  end
end
