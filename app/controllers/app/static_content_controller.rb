# frozen_string_literal: true

module App
  class StaticContentController < BaseController
    def about; end
  end
end
