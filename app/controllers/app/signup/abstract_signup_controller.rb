# frozen_string_literal: true

require 'app/signup/factories/persistence'

module App
  module Signup
    # Abstract controller for signup controllers to inherit from giving them access to
    # #signup_persistence.
    class AbstractSignupController < BaseController
      private

      def signup_persistence
        @signup_persistence ||= Factories::Persistence.new(session).manufacture
      end
    end
  end
end
