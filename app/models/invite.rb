# frozen_string_literal: true

class Invite < ApplicationRecord
  belongs_to :wedding

  validates :wedding, presence: true
end
