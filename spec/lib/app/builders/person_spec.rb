# frozen_string_literal: true

require 'rails_helper'

module App
  module Builders
    describe Person do
      let(:builder) { described_class.new(ar_person) }

      let(:ar_person) do
        ::Person.new(
          id: 2319,
          forename: 'Samuel',
          surname: 'Giles',
          mobile_phone_number: '+447543651833'
        )
      end

      describe '#build' do
        subject(:built_entity) { builder.build }

        specify do
          expect(built_entity).to be_an_instance_of(WeddingInvite::Entities::Person)
          expect(built_entity.id).to eq 2319
          expect(built_entity.forename).to eq 'Samuel'
          expect(built_entity.surname).to eq 'Giles'
          expect(built_entity.mobile_phone_number).to eq '+447543651833'
        end
      end
    end
  end
end
