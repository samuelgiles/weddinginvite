/* global document */
// import ActionCable from 'actioncable'
// window.Cable = ActionCable.createConsumer()

import { Application } from 'stimulus';
import { definitionsFromContext } from 'stimulus/webpack-helpers';
import Rails from 'rails-ujs';
import * as ActiveStorage from 'activestorage';
import Turbolinks from 'turbolinks';
import 'normalize.css';
import 'animate.css';
import '../app/styles.scss';
import '../app/components/Icon/Icon.scss';
import '../app/components/Button/Button.scss';
import '../app/components/SplashScreen/SplashScreen.scss';
import '../app/components/Screen/Screen.scss';
import '../app/components/AboutPage/AboutPage.scss';
import '../app/components/Signup/Signup.scss';
import '../app/components/DomainPreview/DomainPreview.scss';
import '../app/components/IconList/IconList.scss';
import '../app/components/Form/Form.scss';
import '../app/components/Verify/Verify.scss';
import '../app/components/Navigation/Navigation.scss';
import '../app/components/Dashboard/Dashboard.scss';
import '../app/components/Setup/Setup.scss';
import '../app/components/Settings/Settings.scss';

const context = require.context('../app/controllers', true, /\.js$/);

ActiveStorage.start();
Rails.start();
Turbolinks.start();

const application = Application.start();
application.load(definitionsFromContext(context));

Turbolinks.setProgressBarDelay(50);
