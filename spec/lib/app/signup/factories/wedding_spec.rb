# frozen_string_literal: true

require 'rails_helper'

module App
  module Signup
    module Factories
      describe Wedding do
        let(:factory) { described_class.new(stage_one, stage_two, stage_three, settings) }

        let(:stage_one) do
          FactoryBot.build(
            :wedding_invite_signup_stage_one,
            mobile_phone_number: '07543651833'
          )
        end
        let(:stage_two) { FactoryBot.build(:wedding_invite_signup_stage_two) }
        let(:stage_three) { FactoryBot.build(:wedding_invite_signup_stage_three) }
        let(:settings) { Entities::Settings.new(base_domain: 'weddinginvite.io') }

        describe '#manufacture' do
          subject(:wedding) { factory.manufacture }

          specify do
            expect(wedding.name).to eq 'Giles Wedding'
            expect(wedding.date).to eq Date.parse('6th July 2030')
            expect(wedding.primary_domain.hostname).to eq 'giles-wedding.weddinginvite.io'
            expect(wedding.couple.person_a.attributes).to include(
              'forename' => 'Samuel',
              'surname' => 'Giles',
              'mobile_phone_number' => '+44 7543 651833'
            )
            expect(wedding.couple.person_b.attributes).to include(
              'forename' => 'Stephanie',
              'surname' => 'Collins',
              'mobile_phone_number' => nil
            )
          end
        end
      end
    end
  end
end
