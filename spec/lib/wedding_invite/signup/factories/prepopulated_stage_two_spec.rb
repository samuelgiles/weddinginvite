# frozen_string_literal: true

require 'wedding_invite/signup/factories/prepopulated_stage_two'

module WeddingInvite
  module Signup
    module Factories
      describe PrepopulatedStageTwo do
        let(:factory) { described_class.new(stage_one_entity) }
        let(:stage_one_entity) do
          FactoryBot.build(:wedding_invite_signup_stage_one)
        end

        describe '#manufacture' do
          subject(:manufacture) { factory.manufacture }

          let(:stage_two_entity) { instance_double(Entities::StageTwo) }

          before do
            expect(Entities::StageTwo)
              .to receive(:new)
              .with(name: 'Giles Wedding', domain: 'giles-wedding')
              .and_return(stage_two_entity)
          end

          it { is_expected.to eq stage_two_entity }
        end
      end
    end
  end
end
