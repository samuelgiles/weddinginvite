# frozen_string_literal: true

require 'dry/monads/result'
require 'active_support/concern'
require 'active_model'
require 'phonelib'

module Clean
  # Wraps ActiveModel::Validations up to provide a Result with either Success with the
  # object being validated or an ActiveModel::Errors object in a Failure.
  class AbstractValidator
    extend ActiveModel::Naming
    include ActiveModel::Validations

    def initialize(validatable_object, context = {})
      @validatable_object = validatable_object
      @context = context
    end

    def result
      valid? ? Dry::Monads::Success(@validatable_object) : Dry::Monads::Failure(errors)
    end

    def self.performs_validations_on(*attribute_names)
      delegate *attribute_names, to: :@validatable_object
    end
  end
end
