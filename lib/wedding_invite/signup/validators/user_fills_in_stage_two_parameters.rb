# frozen_string_literal: true

require 'clean/abstract_validator'

module WeddingInvite
  module Signup
    module Validators
      class UserFillsInStageTwoParameters < Clean::AbstractValidator
        performs_validations_on :name,
                                :domain

        validates :name,
                  :domain,
                  presence: true

        validate :ensure_domain_is_available

        private

        def ensure_domain_is_available
          return true if signup_persistence.domain_is_available?(domain)
          errors.add(:domain, "isn't available")
        end

        def signup_persistence
          @context.fetch(:signup_persistence)
        end
      end
    end
  end
end
