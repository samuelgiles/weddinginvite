# frozen_string_literal: true

module App
  module Signup
    module Commands
      # Serializes stage one into the session
      class StoreStageOneInSession
        def initialize(session, stage_one)
          @session = session
          @stage_one = stage_one
        end

        STAGE_ONE_SESSION_KEY = :signup_stage_one

        def execute
          @session[STAGE_ONE_SESSION_KEY] = StageOneSerializer.new(@stage_one).to_h
        end

        private

        # Produces a hash suitable for storing stage one in the session
        class StageOneSerializer
          extend Forwardable

          def initialize(stage_one)
            @stage_one = stage_one
          end

          def to_h
            {
              forename: forename,
              surname: surname,
              mobile_phone_number: mobile_phone_number,
              partner_forename: partner_forename,
              partner_surname: partner_surname,
              partner_mobile_phone_number: partner_mobile_phone_number
            }
          end

          private

          def_delegators :@stage_one,
                         :forename,
                         :surname,
                         :mobile_phone_number,
                         :partner_forename,
                         :partner_surname,
                         :partner_mobile_phone_number
        end
      end
    end
  end
end
