# frozen_string_literal: true

require 'dry/monads/maybe'
require 'wedding_invite/authentication/interfaces/session_manager'

module App
  module Authentication
    module Services
      class CookieBasedSessionManager
        implements_interface WeddingInvite::Authentication::Interfaces::SessionManager

        def initialize(cookies)
          @cookies = cookies
        end

        def use_login_token(login_token)
          @cookies.encrypted.permanent[:login_token] = login_token
        end

        def current_user
          Dry::Monads::Maybe(current_login_session).bind do |login_session|
            person_entity = Builders::Person.new(login_session.person).build
            Dry::Monads::Maybe(person_entity)
          end
        end

        def current_wedding
          Dry::Monads::Maybe(current_login_session).bind do |login_session|
            wedding_entity = Builders::Wedding.new(login_session.person.couple.wedding).build
            Dry::Monads::Maybe(wedding_entity)
          end
        end

        private

        def current_login_token
          @cookies.encrypted.permanent[:login_token]
        end

        def current_login_session
          LoginSession.valid.find_by(token: current_login_token)
        end
      end
    end
  end
end
