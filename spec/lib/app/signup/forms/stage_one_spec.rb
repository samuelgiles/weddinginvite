# frozen_string_literal: true

require 'app/signup/forms/stage_one'

module App
  module Signup
    module Forms
      describe StageOne do
        let(:form) { described_class.new(params: params) }

        let(:params) do
          WeddingInvite::Signup::UseCases::UserFillsInStageOne::Parameters.new(
            forename: 'Samuel',
            surname: 'Giles',
            mobile_phone_number: '07543651833',
            partner_forename: 'Stephanie',
            partner_surname: 'Collins',
            partner_mobile_phone_number: '07807201601'
          )
        end

        before do
          Phonelib.default_country = 'GB'
        end

        describe '#to_parameter_object' do
          subject(:parameter_object) { form.to_parameter_object }

          specify do
            expect(parameter_object.mobile_phone_number).to eq '+44 7543 651833'
            expect(parameter_object.partner_mobile_phone_number).to eq '+44 7807 201601'
          end
        end
      end
    end
  end
end
