# frozen_string_literal: true

Phonelib.default_country = Rails.configuration.x.default_country_code_for_phone_number_validation
