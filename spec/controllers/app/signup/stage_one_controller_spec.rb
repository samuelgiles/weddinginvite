# frozen_string_literal: true

require 'rails_helper'

module App
  module Signup
    describe StageOneController do
      describe '#new' do
        subject(:make_request) { get :new }

        let(:stage_one_form_factory) { instance_double(Factories::StageOneForm) }
        let(:stage_one_form) { instance_double(Forms::StageOne) }
        let(:signup_persistence_factory) { instance_double(Factories::Persistence) }
        let(:signup_persistence) { instance_double(Services::Persistence) }

        before do
          expect(Factories::Persistence)
            .to receive(:new)
            .with(controller.session)
            .and_return(signup_persistence_factory)
          expect(signup_persistence_factory).to receive(:manufacture).and_return(signup_persistence)
          expect(Factories::StageOneForm)
            .to receive(:new)
            .with(signup_persistence)
            .and_return(stage_one_form_factory)
          expect(stage_one_form_factory).to receive(:manufacture).and_return(stage_one_form)
        end

        specify do
          make_request
          expect(assigns[:form]).to eq stage_one_form
        end
      end

      describe '#create' do
        subject(:make_request) { post :create, params: params }

        let(:params) do
          {
            app_signup_forms_stage_one: permitted_params
          }
        end
        let(:permitted_params) do
          {
            forename: 'Samuel',
            surname: 'Giles',
            mobile_phone_number: '+447543651833',
            partner_forename: 'Stephanie',
            partner_surname: 'Collins',
            partner_mobile_phone_number: '01234455'
          }
        end
        let(:signup_persistence_factory) { instance_double(Factories::Persistence) }
        let(:signup_persistence) { instance_double(Services::Persistence) }
        let(:user_fills_in_stage_one_use_case) do
          instance_double(WeddingInvite::Signup::UseCases::UserFillsInStageOne)
        end
        let(:user_fills_in_stage_one_parameters) do
          instance_double(WeddingInvite::Signup::UseCases::UserFillsInStageOne::Parameters)
        end
        let(:stage_one_form) { instance_double(Forms::StageOne) }

        before do
          expect(Factories::Persistence)
            .to receive(:new)
            .with(controller.session)
            .and_return(signup_persistence_factory)
          expect(signup_persistence_factory).to receive(:manufacture).and_return(signup_persistence)
          expect(Forms::StageOne)
            .to receive(:new)
            .with(params: permitted_params)
            .and_return(stage_one_form)
          expect(stage_one_form)
            .to receive(:to_parameter_object)
            .and_return(user_fills_in_stage_one_parameters)
          expect(WeddingInvite::Signup::UseCases::UserFillsInStageOne)
            .to receive(:new)
            .with(user_fills_in_stage_one_parameters, signup_persistence)
            .and_return(user_fills_in_stage_one_use_case)
          expect(user_fills_in_stage_one_use_case)
            .to receive(:result)
            .and_return(user_fills_in_stage_one_result)
        end

        context 'when use case result is successful' do
          let(:user_fills_in_stage_one_result) { Dry::Monads::Success(stage_one_entity) }
          let(:stage_one_entity) { instance_double(WeddingInvite::Signup::Entities::StageOne) }

          specify do
            make_request
            expect(response).to redirect_to(new_app_signup_stage_two_path)
          end
        end

        context 'when use case result is unsuccessful' do
          let(:user_fills_in_stage_one_result) { Dry::Monads::Failure(errors) }
          let(:errors) { instance_double(Clean::Errors) }
          let(:stage_one_form_with_errors) { instance_double(Forms::StageOne) }

          before do
            expect(stage_one_form)
              .to receive(:with_errors)
              .with(errors)
              .and_return(stage_one_form_with_errors)
          end

          specify do
            make_request
            expect(assigns[:form]).to eq stage_one_form_with_errors
            expect(response).to render_template(:new)
          end
        end
      end
    end
  end
end
