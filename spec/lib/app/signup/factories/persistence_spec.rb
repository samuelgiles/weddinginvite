# frozen_string_literal: true

require 'rails_helper'

module App
  module Signup
    module Factories
      describe Persistence do
        let(:factory) { described_class.new(session) }

        let(:session) { instance_double(ActionDispatch::Session::AbstractStore) }

        describe '#manufacture' do
          subject(:manufacture) { factory.manufacture }

          let(:persistence) { instance_double(Services::Persistence) }
          let(:settings) { instance_double(Entities::Settings) }

          before do
            expect(Entities::Settings)
              .to receive(:new)
              .with(base_domain: 'localhost')
              .and_return(settings)
            expect(Services::Persistence)
              .to receive(:new)
              .with(session, settings)
              .and_return(persistence)
          end

          it { is_expected.to eq persistence }
        end
      end
    end
  end
end
