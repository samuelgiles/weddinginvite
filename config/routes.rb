# frozen_string_literal: true

Rails.application.routes.draw do
  scope module: 'app', as: 'app' do
    root to: 'app#index'

    resources :sessions, only: %i[new create]
    resources :verifications, only: %i[new create]
    resource :setup, controller: 'setup', only: %i[show]
    resources :settings, only: %i[index show]


    namespace :signup do
      resource :stage_one, controller: 'stage_one', only: %i[new create]
      resource :stage_two, controller: 'stage_two', only: %i[new create]
      resource :stage_three, controller: 'stage_three', only: %i[new create]
      resource :complete, controller: 'complete', only: %i[show]
    end

    scope module: 'static_content' do
      get :about
    end
  end
end
