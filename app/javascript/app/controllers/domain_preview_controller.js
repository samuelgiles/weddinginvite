import { Controller } from 'stimulus';

export default class extends Controller {
  static targets = ['value', 'preview'];

  connect() {
    this.updatePreview();
  }

  updatePreview() {
    const value = this.valueTarget.value;
    if (value.length > 0) {
      const baseDomain = this.data.get("base-domain");
      const valueWithBaseDomain = `${value}.${baseDomain}`;
      this.previewTarget.value = valueWithBaseDomain;
    } else {
      this.previewTarget.value = '';
    }
  }
}
