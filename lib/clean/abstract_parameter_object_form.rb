# frozen_string_literal: true

require 'active_model'
require 'clean/errors'

module Clean
  # Provides an easy way of creating form objects that map to a use case's parameter object
  # and work with Rails form helpers.
  class AbstractParameterObjectForm
    extend ActiveModel::Naming
    include ActiveModel::Conversion

    attr_reader :errors, :context

    def initialize(params: {}, context: {}, errors: Errors.new(nil))
      @params = params.to_h.symbolize_keys
      @context = context
      @errors = errors
    end

    def persisted?
      false
    end

    def to_parameter_object
      @to_parameter_object ||= parameter_object_class.new(parameter_object_hash)
    end

    def with_errors(new_errors, override_params: nil)
      self.class.new(params: override_params || @params, context: @context, errors: new_errors)
    end

    def with_error_message(message, override_params: nil)
      new_errors = Errors.new(nil)
      new_errors.add(:base, message)
      with_errors(new_errors, override_params: override_params)
    end

    def self.acts_as_form_for(parameter_object_class)
      attribute_names = parameter_object_class.schema.keys
      delegate *attribute_names, to: :to_parameter_object

      define_method :parameter_object_class do
        parameter_object_class
      end
    end

    private

    # This can be overridden for forms where the params don't map perfectly
    # to the parameter object
    def parameter_object_hash
      @params
    end
  end
end
