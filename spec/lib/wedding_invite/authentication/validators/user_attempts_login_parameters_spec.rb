# frozen_string_literal: true

require 'wedding_invite/authentication/validators/user_attempts_login_parameters'
require 'wedding_invite/authentication/use_cases/user_attempts_login'
require 'wedding_invite/authentication/interfaces/persistence'

module WeddingInvite
  module Authentication
    module Validators
      describe UserAttemptsLoginParameters do
        let(:validator) do
          described_class.new(
            user_attempts_login_parameters,
            authentication_persistence: authentication_persistence
          )
        end
        let(:user_attempts_login_parameters) do
          instance_double(
            UseCases::UserAttemptsLogin::Parameters,
            phone_number: phone_number
          )
        end
        let(:phone_number) { '+447543651833' }
        let(:authentication_persistence) { instance_double(Interfaces::Persistence) }

        specify do
          expect(described_class).to have_validator(
            ActiveModel::Validations::PresenceValidator,
            %i[phone_number]
          )
        end

        describe '#ensure_phone_number_belongs_to_a_user' do
          subject(:result) { validator.result }

          before do
            expect(authentication_persistence)
              .to receive(:phone_number_belongs_to_a_user?)
              .with('+447543651833')
              .and_return(phone_number_belongs_to_a_user)
          end

          context 'when the phone number exists' do
            let(:phone_number_belongs_to_a_user) { true }

            it { is_expected.to be_a_success_with_value(user_attempts_login_parameters) }
          end

          context 'when the phone number does not exist' do
            let(:phone_number_belongs_to_a_user) { false }

            specify do
              expect(result.failure.full_messages).to contain_exactly(
                "Phone number doesn't appear to be registered to an account"
              )
            end
          end
        end
      end
    end
  end
end
