# frozen_string_literal: true

module App
  module Authentication
    module Queries
      class ValidAuthenticationCode
        def initialize(phone_number, authentication_code)
          @phone_number = phone_number
          @authentication_code = authentication_code
        end

        AUTH_CODES_VALID_FOR_MINUTES = 2

        def exists?
          AuthenticationCode
            .active
            .where(phone_number: @phone_number)
            .where(code: @authentication_code)
            .where('created_at >= ?', AUTH_CODES_VALID_FOR_MINUTES.minutes.ago)
            .exists?
        end
      end
    end
  end
end
