# frozen_string_literal: true

require 'duckface'

module WeddingInvite
  module Messaging
    module Interfaces
      module ServiceProvider
        extend Duckface::ActsAsInterface

        def send_message(message)
          raise NotImplementedError
        end
      end
    end
  end
end
