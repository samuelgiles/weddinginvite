# frozen_string_literal: true

require 'wedding_invite/types'
require 'clean/abstract_use_case'

module WeddingInvite
  module Authentication
    module UseCases
      class UserVerifiesLogin < Clean::AbstractUseCase
        class Parameters < Clean::ParameterObject
          attribute :phone_number, Types::Strict::String.meta(omittable: true)
          attribute :authentication_code, Types::Strict::String.meta(omittable: true)
        end

        def initialize(parameters, authentication_persistence)
          @parameters = parameters
          @authentication_persistence = authentication_persistence
        end

        def result
          result_of_authentication_code_verification.bind do |_|
            Dry::Monads::Success(login_token)
          end
        end

        private

        def login_token
          @authentication_persistence.login_token_for(@parameters.phone_number, 'Web App')
        end

        def result_of_authentication_code_verification
          @authentication_persistence.verify_authentication_code(
            @parameters.phone_number,
            @parameters.authentication_code
          )
        end
      end
    end
  end
end
