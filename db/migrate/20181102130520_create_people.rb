# frozen_string_literal: true

class CreatePeople < ActiveRecord::Migration[5.2]
  def change
    create_table :people do |t|
      t.string :forename, null: false
      t.string :surname, null: false
      t.string :mobile_phone_number

      t.timestamps
    end
    add_index :people, :mobile_phone_number, unique: true
  end
end
