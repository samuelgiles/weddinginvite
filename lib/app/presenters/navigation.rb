# frozen_string_literal: true

module App
  module Presenters
    class Navigation
      include Rails.application.routes.url_helpers

      # Ordered by specifity:
      PATH_TO_ITEM_ID = {
        '/' => :home,
        '/invites' => :invites,
        '/settings' => :settings
      }

      def initialize(current_path)
        @current_path = current_path
      end

      def items
        [
          item(:home, app_root_path, 'app/home.svg', 'Home'),
          # TODO: Points at invites
          item(:invites, app_root_path, 'app/invite.svg', 'Invites'),
          item(:settings, app_settings_path, 'app/settings.svg', 'Settings')
        ]
      end

      private

      def item(id, path, icon, label)
        NavigationItemPresenter.new(
          path,
          id == current_item_id,
          icon,
          label
        )
      end

      DEFAULT_ITEM_ID = :home

      def current_item_id
        @current_item_id ||= begin
          last_match = nil
          PATH_TO_ITEM_ID.each_pair do |target_path, id|
            next unless @current_path.starts_with?(target_path)

            last_match = id
          end
          last_match || DEFAULT_ITEM_ID
        end
      end

      class NavigationItemPresenter
        def initialize(path, active, icon, label)
          @path = path
          @active = active
          @icon = icon
          @label = label
        end

        def active?
          @active
        end

        attr_accessor :path, :icon, :label
      end
    end
  end
end
