# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_11_10_115313) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.string "identifier"
    t.string "subject_type"
    t.bigint "subject_id"
    t.bigint "seen_by_id"
    t.json "metadata", default: {}
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["seen_by_id"], name: "index_activities_on_seen_by_id"
    t.index ["subject_type", "subject_id"], name: "index_activities_on_subject_type_and_subject_id"
  end

  create_table "authentication_codes", force: :cascade do |t|
    t.string "phone_number", null: false
    t.boolean "expired", default: false, null: false
    t.string "code", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["expired"], name: "index_authentication_codes_on_expired"
    t.index ["phone_number", "code"], name: "index_authentication_codes_on_phone_number_and_code"
  end

  create_table "couples", force: :cascade do |t|
    t.bigint "person_a_id"
    t.bigint "person_b_id"
    t.bigint "wedding_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["person_a_id"], name: "index_couples_on_person_a_id"
    t.index ["person_b_id"], name: "index_couples_on_person_b_id"
    t.index ["wedding_id"], name: "index_couples_on_wedding_id"
  end

  create_table "domains", force: :cascade do |t|
    t.string "hostname", null: false
    t.bigint "wedding_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hostname"], name: "index_domains_on_hostname", unique: true
    t.index ["wedding_id"], name: "index_domains_on_wedding_id"
  end

  create_table "events", force: :cascade do |t|
    t.datetime "starts_at", null: false
    t.bigint "venue_id"
    t.string "details", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["venue_id"], name: "index_events_on_venue_id"
  end

  create_table "guests", force: :cascade do |t|
    t.string "forename", null: false
    t.string "surname", null: false
    t.integer "status", default: 0, null: false
    t.datetime "status_updated_at"
    t.bigint "invite_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["invite_id"], name: "index_guests_on_invite_id"
  end

  create_table "invites", force: :cascade do |t|
    t.string "code", null: false
    t.string "notes", null: false
    t.boolean "for_ceremony", default: false, null: false
    t.boolean "for_reception", default: false, null: false
    t.bigint "wedding_id"
    t.datetime "sent_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code", "wedding_id"], name: "index_invites_on_code_and_wedding_id", unique: true
    t.index ["wedding_id"], name: "index_invites_on_wedding_id"
  end

  create_table "login_sessions", force: :cascade do |t|
    t.string "token", null: false
    t.bigint "person_id"
    t.datetime "last_seen"
    t.string "friendly_identifier", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "invalidated", default: false, null: false
    t.index ["invalidated"], name: "index_login_sessions_on_invalidated"
    t.index ["person_id"], name: "index_login_sessions_on_person_id"
    t.index ["token"], name: "index_login_sessions_on_token", unique: true
  end

  create_table "people", force: :cascade do |t|
    t.string "forename", null: false
    t.string "surname", null: false
    t.string "mobile_phone_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["mobile_phone_number"], name: "index_people_on_mobile_phone_number", unique: true
  end

  create_table "venues", force: :cascade do |t|
    t.string "name", null: false
    t.string "address_line_1", null: false
    t.string "address_line_2"
    t.string "city", null: false
    t.string "county", null: false
    t.string "postcode", null: false
    t.string "country", null: false
    t.string "latitude"
    t.string "longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "weddings", force: :cascade do |t|
    t.string "name", null: false
    t.date "date", null: false
    t.string "theme_identifier"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "primary_domain_id"
    t.bigint "ceremony_event_id"
    t.bigint "reception_event_id"
    t.index ["ceremony_event_id"], name: "index_weddings_on_ceremony_event_id"
    t.index ["primary_domain_id"], name: "index_weddings_on_primary_domain_id"
    t.index ["reception_event_id"], name: "index_weddings_on_reception_event_id"
  end

  add_foreign_key "activities", "people", column: "seen_by_id"
  add_foreign_key "couples", "people", column: "person_a_id"
  add_foreign_key "couples", "people", column: "person_b_id"
  add_foreign_key "couples", "weddings"
  add_foreign_key "domains", "weddings"
  add_foreign_key "events", "venues"
  add_foreign_key "guests", "invites"
  add_foreign_key "invites", "weddings"
  add_foreign_key "login_sessions", "people"
  add_foreign_key "weddings", "domains", column: "primary_domain_id"
  add_foreign_key "weddings", "events", column: "ceremony_event_id"
  add_foreign_key "weddings", "events", column: "reception_event_id"
end
