import { Controller } from 'stimulus';

export default class extends Controller {
  static targets = ['header']

  hasHeader() {
    return !!this.headerTarget;
  }

  handleScroll(event) {
    if (!this.hasHeader()) { return; }

    const { currentTarget: { scrollTop } } = event;
    const hasScrolled = scrollTop > 5;

    this.headerTarget.classList.toggle('Screen__header--has-scrolled', hasScrolled);
  }
}
