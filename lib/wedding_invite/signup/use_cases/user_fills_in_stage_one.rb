# frozen_string_literal: true

require 'wedding_invite/types'
require 'wedding_invite/signup/entities/stage_one'
require 'wedding_invite/signup/validators/user_fills_in_stage_one_parameters'
require 'clean/abstract_use_case'

module WeddingInvite
  module Signup
    module UseCases
      class UserFillsInStageOne < Clean::AbstractUseCase
        class Parameters < Clean::ParameterObject
          attribute :forename, Types::Strict::String.meta(omittable: true)
          attribute :surname, Types::Strict::String.meta(omittable: true)
          attribute :mobile_phone_number, Types::Strict::String.meta(omittable: true)
          attribute :partner_forename, Types::Strict::String.meta(omittable: true)
          attribute :partner_surname, Types::Strict::String.meta(omittable: true)
          attribute :partner_mobile_phone_number, Types::Strict::String.meta(omittable: true)
        end

        def initialize(parameters, signup_persistence)
          @parameters = parameters
          @signup_persistence = signup_persistence
        end

        def result
          validator.result.bind do |_|
            @signup_persistence.persist_stage_one(stage_one)
            Dry::Monads::Success(stage_one)
          end
        end

        private

        def stage_one
          @stage_one ||= Entities::StageOne.new(@parameters)
        end

        def validator
          @validator ||= Validators::UserFillsInStageOneParameters.new(
            @parameters,
            signup_persistence: @signup_persistence
          )
        end
      end
    end
  end
end
