# frozen_string_literal: true

require 'wedding_invite/types'
require 'dry/struct'

module WeddingInvite
  module Signup
    module Entities
      # Entity for representing a populated stage one
      class StageOne < Dry::Struct::Value
        attribute :forename, Types::Strict::String
        attribute :surname, Types::Strict::String
        attribute :mobile_phone_number, Types::Strict::String
        attribute :partner_forename, Types::Strict::String
        attribute :partner_surname, Types::Strict::String
        attribute :partner_mobile_phone_number, Types::Strict::String.optional
      end
    end
  end
end
