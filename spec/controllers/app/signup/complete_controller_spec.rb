# frozen_string_literal: true

require 'rails_helper'

module App
  module Signup
    describe CompleteController do
      describe '#show' do
        subject(:make_request) { get :show }

        let(:signup_persistence_factory) { instance_double(Factories::Persistence) }
        let(:signup_persistence) { instance_double(Services::Persistence) }
        let(:complete_signup) { instance_double(WeddingInvite::Signup::Entities::CompleteSignup) }

        before do
          expect(Factories::Persistence)
            .to receive(:new)
            .with(controller.session)
            .and_return(signup_persistence_factory)
          expect(signup_persistence_factory).to receive(:manufacture).and_return(signup_persistence)
          expect(signup_persistence).to receive(:load_complete_signup).and_return(complete_signup)
        end

        specify do
          make_request
          expect(assigns[:complete_signup]).to eq complete_signup
        end
      end
    end
  end
end
