# frozen_string_literal: true

require 'clean/abstract_validator'

module WeddingInvite
  module Authentication
    module Validators
      class UserAttemptsLoginParameters < Clean::AbstractValidator
        performs_validations_on :phone_number

        validates :phone_number,
                  presence: true

        validate :ensure_phone_number_belongs_to_a_user

        private

        def ensure_phone_number_belongs_to_a_user
          return unless phone_number
          return true if phone_number_belongs_to_a_user?
          errors.add(:phone_number, "doesn't appear to be registered to an account")
        end

        def phone_number_belongs_to_a_user?
          authentication_persistence.phone_number_belongs_to_a_user?(phone_number)
        end

        def authentication_persistence
          @context.fetch(:authentication_persistence)
        end
      end
    end
  end
end
