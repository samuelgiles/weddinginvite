# frozen_string_literal: true

require 'wedding_invite/types'
require 'dry/struct'

module WeddingInvite
  module Entities
    class Wedding < Dry::Struct::Value
      attribute :id, Types::Strict::Integer
      attribute :name, Types::Strict::String
      attribute :date, Types::Date
    end
  end
end
