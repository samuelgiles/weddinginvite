# frozen_string_literal: true

class CreateActivities < ActiveRecord::Migration[5.2]
  def change
    create_table :activities do |t|
      t.string :identifier
      t.references :subject, polymorphic: true
      t.references :seen_by, foreign_key: { to_table: :people }
      t.json :metadata, default: {}

      t.timestamps
    end
  end
end
