import { Controller } from 'stimulus';

const loadingClass = 'Verify__code--loading';

export default class extends Controller {
  static targets = ['codeInput', 'codeControl']

  isComplete() {
    const { maxLength, value } = this.codeInputTarget;
    const currentLength = value.length;
    console.log(currentLength);
    return currentLength == maxLength;
  }

  addLoadingClassToControl() {
    this.codeControlTarget.classList.toggle(loadingClass, true);
  }

  disableInput() {
    this.codeInputTarget.readOnly = true;
  }

  alreadyLoading() {
    return this.codeInputTarget.readOnly;
  }

  submitForm() {
    this.element.submit();
  }

  submitWhenComplete() {
    if (!this.isComplete()) { return; }
    if (this.alreadyLoading()) { return; }

    this.addLoadingClassToControl();
    this.disableInput();
    this.submitForm();
  }
}
