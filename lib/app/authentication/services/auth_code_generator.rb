# frozen_string_literal: true

module App
  module Authentication
    module Services
      class AuthCodeGenerator
        def initialize(digits, seed = Random.new_seed)
          @digits = digits
          @seed = seed
        end

        def generate
          @digits.times.map { random_number }.join
        end

        private

        NUMBERS_UP_TO = 10

        def random_number
          random.rand(NUMBERS_UP_TO)
        end

        def random
          @random ||= Random.new(@seed)
        end

        private_constant :NUMBERS_UP_TO
      end
    end
  end
end
