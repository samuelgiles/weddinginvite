# frozen_string_literal: true

require 'rails_helper'

module App
  module Authentication
    module Queries
      describe ValidAuthenticationCode do
        let(:query) { described_class.new(phone_number, authentication_code) }

        let(:phone_number) { '+447543651833' }
        let(:authentication_code) { '2319' }

        describe '#exists?' do
          subject(:exists?) { query.exists? }

          let(:authentication_code_relation) do
            instance_double(ActiveRecord::Relation)
          end
          let(:now) { Time.at(1541964600) }
          let(:two_minutes_ago) { Time.at(1541964480) }

          around do |example|
            Timecop.freeze(now) do
              example.run
            end
          end

          before do
            expect(AuthenticationCode).to receive(:active).and_return(authentication_code_relation)
            expect(authentication_code_relation)
              .to receive(:where)
              .with(phone_number: '+447543651833')
              .and_return(authentication_code_relation)
            expect(authentication_code_relation)
              .to receive(:where)
              .with(code: '2319')
              .and_return(authentication_code_relation)
            expect(authentication_code_relation)
              .to receive(:where)
              .with('created_at >= ?', two_minutes_ago)
              .and_return(authentication_code_relation)
            expect(authentication_code_relation)
              .to receive(:exists?)
              .and_return(authentication_code_exists)
          end

          context 'when an authentication code exists' do
            let(:authentication_code_exists) { true }

            it { is_expected.to be true }
          end

          context 'when an authentication code does not exist' do
            let(:authentication_code_exists) { false }

            it { is_expected.to be false }
          end
        end
      end
    end
  end
end
