# frozen_string_literal: true

require 'wedding_invite/types'
require 'dry/struct'

module WeddingInvite
  module Entities
    class Person < Dry::Struct::Value
      attribute :id, Types::Strict::Integer
      attribute :forename, Types::Strict::String
      attribute :surname, Types::Strict::String
      attribute :mobile_phone_number, Types::Strict::String.optional
    end
  end
end
