# frozen_string_literal: true

require 'rails_helper'

module App
  module Signup
    module Services
      describe Persistence do
        let(:service) { described_class.new(session, settings) }
        let(:session) do
          {}
        end
        let(:settings) do
          Entities::Settings.new(
            base_domain: 'weddinginvite.io'
          )
        end

        it_behaves_like 'it implements', WeddingInvite::Signup::Interfaces::Persistence

        describe '#load_stage_one' do
          subject(:load_stage_one) { service.load_stage_one }

          context 'when there is signup data in the session' do
            let(:signup_stage_one_attributes) do
              FactoryBot.attributes_for(:wedding_invite_signup_stage_one)
            end
            let(:session) do
              {
                signup_stage_one: signup_stage_one_attributes.stringify_keys
              }
            end
            let(:stage_one_entity) { instance_double(WeddingInvite::Signup::Entities::StageOne) }

            before do
              expect(WeddingInvite::Signup::Entities::StageOne)
                .to receive(:new)
                .with(signup_stage_one_attributes)
                .and_return(stage_one_entity)
            end

            it { is_expected.to be_a_success_with_value(stage_one_entity) }
          end

          context 'when there is no existing signup data in the session' do
            let(:session) do
              {}
            end

            it { is_expected.to be_a_failure_with_value(:no_signup_data) }
          end
        end

        describe '#persist_stage_one' do
          subject(:persist_stage_one) { service.persist_stage_one(stage_one_entity) }

          let(:stage_one_entity) { instance_double(WeddingInvite::Signup::Entities::StageOne) }
          let(:store_stage_one_in_session_command) do
            instance_double(Commands::StoreStageOneInSession)
          end

          before do
            expect(Commands::StoreStageOneInSession)
              .to receive(:new)
              .with(session, stage_one_entity)
              .and_return(store_stage_one_in_session_command)
          end

          specify do
            expect(store_stage_one_in_session_command).to receive(:execute)
            expect(persist_stage_one).to eq stage_one_entity
          end
        end

        describe '#persist_stage_two' do
          subject(:persist_stage_two) { service.persist_stage_two(stage_two) }

          let(:stage_two) { instance_double(WeddingInvite::Signup::Entities::StageTwo) }
          let(:store_stage_two_in_session_command) do
            instance_double(Commands::StoreStageTwoInSession)
          end

          before do
            expect(Commands::StoreStageTwoInSession)
              .to receive(:new)
              .with(session, stage_two)
              .and_return(store_stage_two_in_session_command)
          end

          specify do
            expect(store_stage_two_in_session_command).to receive(:execute)
            expect(persist_stage_two).to eq stage_two
          end
        end

        describe '#base_domain' do
          subject(:base_domain) { service.base_domain }

          it { is_expected.to eq 'weddinginvite.io' }
        end

        describe '#domain_is_available?' do
          subject(:domain_is_available?) { service.domain_is_available?(domain) }

          let(:domain) { 'giles-wedding' }
          let(:domain_relation) { instance_double(ActiveRecord::Relation) }

          before do
            expect(Domain)
              .to receive(:where)
              .with(hostname: 'giles-wedding.weddinginvite.io')
              .and_return(domain_relation)
            expect(domain_relation).to receive(:exists?).and_return(domain_exists)
          end

          context "when a domain already exists with matching hostname" do
            let(:domain_exists) { true }

            it { is_expected.to be false }
          end

          context "when a domain doesn't already exist with a matching hostname" do
            let(:domain_exists) { false }

            it { is_expected.to be true }
          end
        end

        describe '#persist_signup' do
          subject(:persist_signup) { service.persist_signup(stage_one, stage_two, stage_three) }

          let(:stage_one) { FactoryBot.build(:wedding_invite_signup_stage_one) }
          let(:stage_two) { FactoryBot.build(:wedding_invite_signup_stage_two) }
          let(:stage_three) { FactoryBot.build(:wedding_invite_signup_stage_three) }
          let(:wedding_factory) { instance_double(Factories::Wedding) }
          let(:ar_wedding) { instance_double(::Wedding) }

          before do
            expect(Factories::Wedding)
              .to receive(:new)
              .with(stage_one, stage_two, stage_three, settings)
              .and_return(wedding_factory)
            expect(wedding_factory).to receive(:manufacture).and_return(ar_wedding)
            expect(ar_wedding).to receive(:id).and_return(2319)
          end

          specify do
            expect(ar_wedding).to receive(:save!)
            persist_signup
            expect(session[:completed_signup_id]).to eq 2319
          end
        end

        describe '#load_complete_signup' do
          subject(:complete_signup) { service.load_complete_signup }

          let(:person_b) { instance_double(Person, forename: 'Stephanie') }
          let(:couple) { instance_double(Couple, person_b: person_b) }
          let(:wedding) { instance_double(Wedding, couple: couple) }

          before do
            expect(session).to receive(:fetch).with(:completed_signup_id).and_return(2319)
            expect(Wedding).to receive(:find).with(2319).and_return(wedding)
          end

          specify do
            expect(complete_signup)
              .to be_an_instance_of(WeddingInvite::Signup::Entities::CompleteSignup)
            expect(complete_signup.partner_forename).to eq 'Stephanie'
            expect(complete_signup.login_token).to eq 'hello world'
          end
        end

        describe '#mobile_phone_number_is_available?' do
          subject { service.mobile_phone_number_is_available?(mobile_phone_number) }

          let(:mobile_phone_number) { '+447543651833' }
          let(:person_relation) { instance_double(ActiveRecord::Relation) }

          before do
            expect(::Person)
              .to receive(:where)
              .with(mobile_phone_number: '+447543651833')
              .and_return(person_relation)
            expect(person_relation).to receive(:exists?).and_return(already_exists)
          end

          context 'when phone number is available' do
            let(:already_exists) { false }

            it { is_expected.to be true }
          end

          context 'when phone number is unavailable' do
            let(:already_exists) { true }

            it { is_expected.to be false }
          end
        end
      end
    end
  end
end
