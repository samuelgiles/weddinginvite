# frozen_string_literal: true

require 'wedding_invite/signup/use_cases/user_fills_in_stage_two'
require 'wedding_invite/signup/interfaces/persistence'
require 'clean/errors'

module WeddingInvite
  module Signup
    module UseCases
      describe UserFillsInStageTwo do
        let(:use_case) { described_class.new(parameters, signup_persistence) }

        let(:parameters) { described_class::Parameters.new(parameters_hash) }
        let(:parameters_hash) { Hash.new }
        let(:signup_persistence) { instance_double(Interfaces::Persistence) }

        describe '#result' do
          subject(:result) { use_case.result }

          let(:validator) do
            instance_double(Validators::UserFillsInStageTwoParameters)
          end

          before do
            expect(Validators::UserFillsInStageTwoParameters)
              .to receive(:new)
              .with(parameters, signup_persistence: signup_persistence)
              .and_return(validator)
            expect(validator)
              .to receive(:result)
              .and_return(validator_result)
          end

          context 'when the parameters are valid' do
            let(:validator_result) { Dry::Monads::Success(parameters) }
            let(:stage_two_entity) { instance_double(Entities::StageTwo) }

            before do
              expect(Entities::StageTwo)
                .to receive(:new)
                .with(parameters)
                .and_return(stage_two_entity)
              expect(signup_persistence)
                .to receive(:persist_stage_two)
                .with(stage_two_entity)
                .and_return(stage_two_entity)
            end

            it { is_expected.to be_a_success_with_value(stage_two_entity) }
          end

          context 'when the parameters are not valid' do
            let(:validator_result) { Dry::Monads::Failure(errors) }
            let(:errors) { instance_double(Clean::Errors) }

            it { is_expected.to be_a_failure_with_value(errors) }
          end
        end
      end
    end
  end
end
