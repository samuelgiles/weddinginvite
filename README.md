# WeddingInvite.io

Website for managing a wedding invite list.

## Development

Run `rails s` to spinup a server and run the Webpack build server with `bin/webpack-dev-server`.

## Deployment

WeddingInvite.io is hosted on Zeit Now, deployment is automatically handled as part of CI. Pushes to the `master` branch will deploy to staging, pushes to `stable` will deploy to production.
