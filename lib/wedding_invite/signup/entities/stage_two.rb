# frozen_string_literal: true

require 'wedding_invite/types'
require 'dry/struct'

module WeddingInvite
  module Signup
    module Entities
      # Entity for representing a populated stage two
      class StageTwo < Dry::Struct::Value
        attribute :name, Types::Strict::String
        attribute :domain, Types::Strict::String
      end
    end
  end
end
