class CreateCouples < ActiveRecord::Migration[5.2]
  def change
    create_table :couples do |t|
      t.references :person_a, foreign_key: { to_table: :people }
      t.references :person_b, foreign_key: { to_table: :people }
      t.references :wedding, foreign_key: true

      t.timestamps
    end
  end
end
