# frozen_string_literal: true

require 'wedding_invite/messaging/service_providers/plivo'
require 'app/factories/plivo_client'

module App
  module Factories
    class PlivoMessagingServiceProvider
      def initialize(credentials = Rails.application.credentials)
        @credentials = credentials
      end

      def manufacture
        WeddingInvite::Messaging::ServiceProviders::Plivo.new(
          plivo_client,
          source_number
        )
      end

      private

      def plivo_client
        PlivoClient.new(@credentials).manufacture
      end

      def source_number
        @credentials.dig(:plivo, :source_number)
      end
    end
  end
end
