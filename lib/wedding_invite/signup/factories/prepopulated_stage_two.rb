# frozen_string_literal: true

require 'active_support/inflector'
require 'wedding_invite/signup/entities/stage_two'

module WeddingInvite
  module Signup
    module Factories
      # Produces stage two based on stage one
      class PrepopulatedStageTwo
        def initialize(stage_one)
          @stage_one = stage_one
        end

        def manufacture
          Entities::StageTwo.new(
            name: suggested_wedding_name,
            domain: suggested_domain
          )
        end

        private

        def suggested_wedding_name
          "#{@stage_one.surname} Wedding"
        end

        def suggested_domain
          ActiveSupport::Inflector.parameterize(suggested_wedding_name)
        end
      end
    end
  end
end
