# frozen_string_literal: true

require 'wedding_invite/authentication/use_cases/user_verifies_login'
require 'wedding_invite/authentication/interfaces/persistence'
require 'clean/errors'

module WeddingInvite
  module Authentication
    module UseCases
      describe UserVerifiesLogin do
        let(:use_case) do
          described_class.new(parameters, authentication_persistence)
        end

        let(:parameters) { described_class::Parameters.new(parameters_hash) }
        let(:parameters_hash) do
          { phone_number: '+447543651833', authentication_code: '2319' }
        end
        let(:authentication_persistence) { instance_double(Interfaces::Persistence) }

        describe '#result' do
          subject(:result) { use_case.result }

          before do
            expect(authentication_persistence)
              .to receive(:verify_authentication_code)
              .with('+447543651833', '2319')
              .and_return(result_of_authentication_code_verification)
          end

          context 'when the code is valid' do
            let(:result_of_authentication_code_verification) do
              Dry::Monads::Success(true)
            end

            let(:login_token) { 'super-secret-login-token' }

            before do
              expect(authentication_persistence)
                .to receive(:login_token_for)
                .with('+447543651833', 'Web App')
                .and_return('super-secret-login-token')
            end

            it { is_expected.to be_a_success_with_value('super-secret-login-token') }
          end

          context 'when the code is invalid' do
            let(:result_of_authentication_code_verification) { Dry::Monads::Failure('Failed to verify code') }

            it { is_expected.to be_a_failure_with_value('Failed to verify code') }
          end
        end
      end
    end
  end
end
