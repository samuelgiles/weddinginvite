# frozen_string_literal: true

require 'rails_helper'

module App
  describe SettingsController do
    describe '#index' do
      subject(:make_request) { get :index }

      it_behaves_like 'a controller action that requires users to be logged in'

      context 'when logged in' do
        before do
          expect(controller).to receive(:logged_in?).and_return(true)
          expect(controller).to receive(:prepare_navigation)
        end

        specify do
          make_request
          expect(response).to be_successful
        end
      end
    end
  end
end
