# frozen_string_literal: true

require 'wedding_invite/signup/validators/user_fills_in_stage_three_parameters'
require 'wedding_invite/signup/use_cases/user_fills_in_stage_three'
require 'wedding_invite/signup/interfaces/persistence'

module WeddingInvite
  module Signup
    module Validators
      describe UserFillsInStageThreeParameters do
        let(:validator) do
          described_class.new(user_fills_in_stage_three_parameters)
        end
        let(:date) { DateTime.parse('6th July 2019') }
        let(:terms_marked_as_agreed) { true }
        let(:user_fills_in_stage_three_parameters) do
          UseCases::UserFillsInStageThree::Parameters.new(
            date: date,
            terms_agreed: terms_marked_as_agreed
          )
        end
        let(:now) { DateTime.parse('10th May 2019') }

        around do |example|
          Timecop.freeze(now) do
            example.run
          end
        end

        specify do
          expect(described_class).to have_validator(
            ActiveModel::Validations::PresenceValidator,
            %i[date]
          )
        end

        describe '#date_must_be_in_future' do
          subject(:result) { validator.result }

          context 'when the date is in the future' do
            let(:date) { DateTime.parse('6th July 2019') }

            it { is_expected.to be_a_success_with_value(user_fills_in_stage_three_parameters) }
          end

          context 'when the date is in the past' do
            let(:date) { DateTime.parse('3rd May 2019') }

            specify do
              expect(result.failure.full_messages).to contain_exactly(
                "Date cannot be in the past"
              )
            end
          end
        end

        describe '#terms_must_be_agreed' do
          subject(:result) { validator.result }

          context 'when the terms are marked as agreed' do
            let(:terms_marked_as_agreed) { true }

            it { is_expected.to be_a_success_with_value(user_fills_in_stage_three_parameters) }
          end

          context 'when the terms are not marked as agreed' do
            let(:terms_marked_as_agreed) { false }

            specify do
              expect(result.failure.full_messages).to contain_exactly(
                "Terms agreed must be checked"
              )
            end
          end
        end
      end
    end
  end
end
