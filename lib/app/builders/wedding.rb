# frozen_string_literal: true

module App
  module Builders
    class Wedding < AbstractActiveRecordEntityBuilder
      acts_as_builder_for_entity WeddingInvite::Entities::Wedding
    end
  end
end
