# frozen_string_literal: true

class AuthenticationCode < ApplicationRecord
  validates :phone_number, :code, presence: true

  scope :active, -> { where(expired: false) }
end
