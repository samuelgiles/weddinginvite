# frozen_string_literal: true

require 'clean/abstract_parameter_object_form'
require 'wedding_invite/signup/use_cases/user_fills_in_stage_two'

module App
  module Signup
    module Forms
      # Form object for the UserFillsInStageOne use case
      class StageTwo < Clean::AbstractParameterObjectForm
        acts_as_form_for WeddingInvite::Signup::UseCases::UserFillsInStageTwo::Parameters
      end
    end
  end
end
