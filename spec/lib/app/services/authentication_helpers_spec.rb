# frozen_string_literal: true

require 'app/services/authentication_helpers'
require 'dry/monads/maybe'
require 'wedding_invite/entities/person'
require 'wedding_invite/entities/wedding'
require 'wedding_invite/authentication/interfaces/session_manager'

module App
  module Services
    describe AuthenticationHelpers do
      let(:service) { described_class.new(session_manager) }

      let(:session_manager) do
        instance_double(WeddingInvite::Authentication::Interfaces::SessionManager)
      end

      describe '#current_user' do
        subject(:current_user) { service.current_user }

        let(:maybe_current_user) do
          Dry::Monads::Maybe(person_entity)
        end

        before do
          expect(session_manager)
            .to receive(:current_user)
            .and_return(maybe_current_user)
        end

        context 'when the user is logged in' do
          let(:person_entity) { instance_double(WeddingInvite::Entities::Person) }

          it { is_expected.to eq person_entity }
        end

        context 'when the user is logged out' do
          let(:person_entity) { nil }

          specify do
            expect { current_user }.to raise_error(Dry::Monads::UnwrapError)
          end
        end
      end

      describe '#current_wedding' do
        subject(:current_wedding) { service.current_wedding }

        let(:maybe_current_wedding) do
          Dry::Monads::Maybe(wedding_entity)
        end

        before do
          expect(session_manager)
            .to receive(:current_wedding)
            .and_return(maybe_current_wedding)
        end

        context 'when the user is logged in' do
          let(:wedding_entity) { instance_double(WeddingInvite::Entities::Wedding) }

          it { is_expected.to eq wedding_entity }
        end

        context 'when the user is logged out' do
          let(:wedding_entity) { nil }

          specify do
            expect { current_wedding }.to raise_error(Dry::Monads::UnwrapError)
          end
        end
      end

      describe '#logged_in?' do
        subject(:logged_in?) { service.logged_in? }

        let(:maybe_current_user) do
          Dry::Monads::Maybe(person_entity)
        end

        before do
          expect(session_manager)
            .to receive(:current_user)
            .and_return(maybe_current_user)
        end

        context 'when the user is logged in' do
          let(:person_entity) { instance_double(WeddingInvite::Entities::Person) }

          it { is_expected.to be true }
        end

        context 'when the user is logged out' do
          let(:person_entity) { nil }

          it { is_expected.to be false }
        end
      end
    end
  end
end
