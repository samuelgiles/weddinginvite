# frozen_string_literal: true

require 'rails_helper'

module App
  module Signup
    describe StageTwoController do
      describe '#new' do
        subject(:make_request) { get :new }

        let(:stage_two_form_factory) { instance_double(Factories::StageTwoForm) }
        let(:stage_two_form) { instance_double(Forms::StageTwo) }
        let(:signup_persistence_factory) { instance_double(Factories::Persistence) }
        let(:signup_persistence) { instance_double(Services::Persistence) }

        before do
          expect(Factories::Persistence)
            .to receive(:new)
            .with(controller.session)
            .and_return(signup_persistence_factory)
          expect(signup_persistence_factory).to receive(:manufacture).and_return(signup_persistence)
          expect(Factories::StageTwoForm)
            .to receive(:new)
            .with(signup_persistence)
            .and_return(stage_two_form_factory)
          expect(stage_two_form_factory).to receive(:manufacture).and_return(stage_two_form)
        end

        specify do
          make_request
          expect(assigns[:form]).to eq stage_two_form
        end
      end

      describe '#create' do
        subject(:make_request) { post :create, params: params }

        let(:params) do
          {
            app_signup_forms_stage_two: permitted_params
          }
        end
        let(:permitted_params) do
          {
            name: 'Giles Wedding',
            domain: 'giles-wedding'
          }
        end
        let(:signup_persistence_factory) { instance_double(Factories::Persistence) }
        let(:signup_persistence) { instance_double(Services::Persistence) }
        let(:user_fills_in_stage_two_use_case) do
          instance_double(WeddingInvite::Signup::UseCases::UserFillsInStageTwo)
        end
        let(:user_fills_in_stage_two_parameters) do
          instance_double(WeddingInvite::Signup::UseCases::UserFillsInStageTwo::Parameters)
        end
        let(:stage_two_form) { instance_double(Forms::StageTwo) }

        before do
          expect(Factories::Persistence)
            .to receive(:new)
            .with(controller.session)
            .and_return(signup_persistence_factory)
          expect(signup_persistence_factory).to receive(:manufacture).and_return(signup_persistence)
          expect(Forms::StageTwo)
            .to receive(:new)
            .with(params: permitted_params)
            .and_return(stage_two_form)
          expect(stage_two_form)
            .to receive(:to_parameter_object)
            .and_return(user_fills_in_stage_two_parameters)
          expect(WeddingInvite::Signup::UseCases::UserFillsInStageTwo)
            .to receive(:new)
            .with(user_fills_in_stage_two_parameters, signup_persistence)
            .and_return(user_fills_in_stage_two_use_case)
          expect(user_fills_in_stage_two_use_case)
            .to receive(:result)
            .and_return(user_fills_in_stage_two_result)
        end

        context 'when use case result is successful' do
          let(:user_fills_in_stage_two_result) { Dry::Monads::Success(stage_two_entity) }
          let(:stage_two_entity) { instance_double(WeddingInvite::Signup::Entities::StageTwo) }

          specify do
            make_request
            expect(response).to redirect_to(new_app_signup_stage_three_path)
          end
        end

        context 'when use case result is unsuccessful' do
          let(:user_fills_in_stage_two_result) { Dry::Monads::Failure(errors) }
          let(:errors) { instance_double(Clean::Errors) }
          let(:stage_two_form_with_errors) { instance_double(Forms::StageTwo) }

          before do
            expect(stage_two_form)
              .to receive(:with_errors)
              .with(errors)
              .and_return(stage_two_form_with_errors)
          end

          specify do
            make_request
            expect(assigns[:form]).to eq stage_two_form_with_errors
            expect(response).to render_template(:new)
          end
        end
      end
    end
  end
end
