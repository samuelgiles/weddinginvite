# frozen_string_literal: true

class Person < ApplicationRecord
  has_one :couple_as_person_a, class_name: Couple.name, foreign_key: :person_a_id
  has_one :couple_as_person_b, class_name: Couple.name, foreign_key: :person_b_id
  has_many :login_sessions
  has_many :seen_activities,
           class_name: Activity.name,
           foreign_key: :seen_by_id,
           dependent: :destroy

  validates :forename, :surname, presence: true
  validates :mobile_phone_number, uniqueness: true

  def couple
    Couple.where(person_a_id: id).or(Couple.where(person_b_id: id)).first!
  end
end
