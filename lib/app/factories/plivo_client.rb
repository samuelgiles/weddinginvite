# frozen_string_literal: true

require 'plivo'

module App
  module Factories
    class PlivoClient
      def initialize(credentials = Rails.application.credentials)
        @credentials = credentials
      end

      def manufacture
        Plivo::RestClient.new(auth_id, auth_token)
      end

      private

      def plivo_credentials
        @credentials.plivo
      end

      def auth_id
        plivo_credentials.fetch(:auth_id)
      end

      def auth_token
        plivo_credentials.fetch(:auth_token)
      end
    end
  end
end
