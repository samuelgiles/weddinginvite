# frozen_string_literal: true

class Domain < ApplicationRecord
  belongs_to :wedding

  validates :wedding, :hostname, presence: true
  validates :hostname, uniqueness: true
end
