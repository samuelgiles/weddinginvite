# frozen_string_literal: true

require 'app/signup/entities/settings'
require 'app/signup/services/persistence'

module App
  module Signup
    module Factories
      class Persistence
        def initialize(session)
          @session = session
        end

        def manufacture
          Services::Persistence.new(@session, settings)
        end

        private

        def settings
          @settings ||= Entities::Settings.new(
            base_domain: Rails.configuration.x.base_domain
          )
        end
      end
    end
  end
end
