# frozen_string_literal: true

require 'clean/parameter_object'
require 'dry/monads/result'

module Clean
  class AbstractUseCase
    def initialize
      raise NotImplementedError
    end

    def result
      raise NotImplementedError
    end
  end
end
