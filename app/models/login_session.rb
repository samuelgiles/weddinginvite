# frozen_string_literal: true

class LoginSession < ApplicationRecord
  belongs_to :person

  validates :person, presence: true

  scope :valid, -> { where(invalidated: false) }
end
