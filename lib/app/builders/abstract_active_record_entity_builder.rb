# frozen_string_literal: true

module App
  module Builders
    # Helps to take an instance of an AR model and wrap it up in an Entity
    class AbstractActiveRecordEntityBuilder
      def self.acts_as_builder_for_entity(entity_class)
        define_method :entity_class do
          entity_class
        end

        private :entity_class
      end

      def initialize(ar_model_instance)
        @ar_model_instance = ar_model_instance
      end

      def build
        entity_class.new(all_attributes_for_entity)
      end

      private

      def entity_attribute_names
        @entity_attributes ||= entity_class.schema.keys
      end

      def ar_attributes_for_entity
        @ar_model_instance.attributes.symbolize_keys.slice(*entity_attribute_names)
      end

      def attributes_for_entity
        {}
      end

      def all_attributes_for_entity
        attributes_for_entity.merge(ar_attributes_for_entity)
      end
    end
  end
end
