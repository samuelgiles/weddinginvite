# frozen_string_literal: true

require 'wedding_invite/messaging/interfaces/service_provider'

module WeddingInvite
  module Messaging
    module ServiceProviders
      class Plivo
        implements_interface Interfaces::ServiceProvider

        def initialize(plivo_client, source_number)
          @plivo_client = plivo_client
          @source_number = source_number
        end

        def send_message(message)
          @plivo_client.messages.create(
            @source_number,
            [message.phone_number],
            message.content
          )
        end
      end
    end
  end
end
