# frozen_string_literal: true

require "dry/matcher/result_matcher"
require 'wedding_invite/authentication/use_cases/user_verifies_login'

module App
  class VerificationsController < BaseController
    def new
      @form = Authentication::Forms::Verify.new(
        params: form_params
      )
    end

    def create
      Dry::Matcher::ResultMatcher.call(user_verifies_login_result) do |matcher|
        matcher.success do |login_token|
          session[:verifying_login_for_phone_number] = nil
          session_manager.use_login_token(login_token)
          redirect_to app_root_path
        end

        matcher.failure do |_|
          @form = verify_form.with_error_message(
            "That code doesn't look quite right, re-enter the code or try again.",
            override_params: form_params
          )
          render :new
        end
      end
    end

    private

    PERMITTED_CREATE_ATTRIBUTES = %i[phone_number authentication_code]

    def create_params
      params.require(:app_authentication_forms_verify).permit(*PERMITTED_CREATE_ATTRIBUTES)
    end

    def form_params
      { phone_number: decoded_phone_number }
    end

    def decoded_phone_number
      session.fetch(:verifying_login_for_phone_number)
    end

    def verify_form
      @verify_form ||= Authentication::Forms::Verify.new(params: create_params.to_h)
    end

    def user_verifies_login_parameters
      verify_form.to_parameter_object
    end

    def user_verifies_login_result
      WeddingInvite::Authentication::UseCases::UserVerifiesLogin.new(
        user_verifies_login_parameters,
        authentication_persistence
      ).result
    end
  end
end
