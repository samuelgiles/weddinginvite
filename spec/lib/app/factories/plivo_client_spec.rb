# frozen_string_literal: true

require 'rails_helper'

module App
  module Factories
    describe PlivoClient do
      let(:factory) { described_class.new(credentials) }

      let(:credentials) do
        options = ActiveSupport::OrderedOptions.new
        options[:plivo] = { auth_id: 'secret-id', auth_token: 'secret-token' }
        options
      end

      describe '#manufacture' do
        subject { factory.manufacture }

        let(:plivo_client) { instance_double(Plivo::RestClient) }

        before do
          expect(Plivo::RestClient)
            .to receive(:new)
            .with('secret-id', 'secret-token')
            .and_return(plivo_client)
        end

        it { is_expected.to eq plivo_client }
      end
    end
  end
end
