# frozen_string_literal: true

RSpec::Matchers.define :have_validator do |desired_validator, attributes|
  match do |klass|
    found_validator = klass.validators.find { |validator| validator.is_a?(desired_validator) }
    return false unless found_validator
    found_validator.attributes == attributes
  end

  failure_message do |klass|
    "expected that #{klass} would have #{desired_validator} on #{attributes.map(&:to_s).join(',')}"
  end
end
