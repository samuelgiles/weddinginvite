# frozen_string_literal: true

require 'wedding_invite/entities/wedding'

FactoryBot.define do
  factory :wedding_invite_wedding, class: WeddingInvite::Entities::Wedding do
    transient do
      id { 2319 }
      name { 'Giles Wedding' }
      date { Date.parse('6th July 2019') }
    end

    initialize_with do
      attributes = {
        id: id,
        name: name,
        date: date
      }

      WeddingInvite::Entities::Wedding.new(attributes)
    end
  end
end
