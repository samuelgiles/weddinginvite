# frozen_string_literal: true

require 'phonelib'

module App
  module Signup
    module Factories
      class Wedding
        extend Forwardable

        def initialize(stage_one, stage_two, stage_three, settings)
          @stage_one = stage_one
          @stage_two = stage_two
          @stage_three = stage_three
          @settings = settings
        end

        def manufacture
          ::Wedding.new(
            name: name,
            date: date,
            primary_domain: primary_domain,
            couple: couple
          )
        end

        private

        def_delegators :@stage_one,
                       :forename,
                       :surname,
                       :mobile_phone_number,
                       :partner_forename,
                       :partner_surname,
                       :partner_mobile_phone_number
        def_delegators :@stage_two, :name, :domain
        def_delegators :@stage_three, :date

        def hostname
          [domain, @settings.base_domain].join('.')
        end

        def couple
          Couple.new(person_a: person_a, person_b: person_b)
        end

        def person_a
          Person.new(
            forename: forename,
            surname: surname,
            mobile_phone_number: international_mobile_phone_number
          )
        end

        def international_mobile_phone_number
          Phonelib.parse(mobile_phone_number).international
        end

        def international_partner_mobile_phone_number
          return unless partner_mobile_phone_number
          Phonelib.parse(partner_mobile_phone_number).international
        end

        def person_b
          Person.new(
            forename: partner_forename,
            surname: partner_surname,
            mobile_phone_number: international_partner_mobile_phone_number
          )
        end

        def primary_domain
          Domain.new(hostname: hostname)
        end
      end
    end
  end
end
