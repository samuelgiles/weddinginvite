# frozen_string_literal: true

require 'rails_helper'

module App
  module Services
    describe PerRequestRegistry do
      let(:service) { described_class.new(controller) }

      let(:controller) { instance_double(ApplicationController) }

      describe '#authentication_persistence' do
        subject { service.authentication_persistence }

        let(:authentication_persistence) { instance_double(Authentication::Services::Persistence) }

        before do
          expect(Authentication::Services::Persistence)
            .to receive(:new)
            .and_return(authentication_persistence)
        end

        it { is_expected.to eq authentication_persistence }
      end

      describe '#messaging_service_provider' do
        subject { service.messaging_service_provider }

        let(:plivo_messaging_service_provider_factory) do
          instance_double(Factories::PlivoMessagingServiceProvider)
        end
        let(:plivo_messaing_service_provider) do
          instance_double(WeddingInvite::Messaging::ServiceProviders::Plivo)
        end

        before do
          expect(Factories::PlivoMessagingServiceProvider)
            .to receive(:new)
            .and_return(plivo_messaging_service_provider_factory)
          expect(plivo_messaging_service_provider_factory)
            .to receive(:manufacture)
            .and_return(plivo_messaing_service_provider)
        end

        it { is_expected.to eq plivo_messaing_service_provider }
      end
    end
  end
end
