# frozen_string_literal: true

require 'duckface'

module WeddingInvite
  module Signup
    module Interfaces
      module Persistence
        extend Duckface::ActsAsInterface

        def load_stage_one
          raise NotImplementedError
        end

        def persist_stage_one(_stage_one)
          raise NotImplementedError
        end

        def load_stage_two
          raise NotImplementedError
        end

        def persist_stage_two(_stage_two)
          raise NotImplementedError
        end

        def domain_is_available?(_domain)
          raise NotImplementedError
        end

        def persist_signup(_stage_one, _stage_two, _stage_three)
          raise NotImplementedError
        end

        def base_domain
          raise NotImplementedError
        end

        def load_complete_signup
          raise NotImplementedError
        end

        def mobile_phone_number_is_available?(_mobile_phone_number)
          raise NotImplementedError
        end
      end
    end
  end
end
