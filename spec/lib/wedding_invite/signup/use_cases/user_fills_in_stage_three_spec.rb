# frozen_string_literal: true

require 'wedding_invite/signup/use_cases/user_fills_in_stage_three'
require 'wedding_invite/signup/interfaces/persistence'
require 'clean/errors'

module WeddingInvite
  module Signup
    module UseCases
      describe UserFillsInStageThree do
        let(:use_case) { described_class.new(parameters, signup_persistence) }

        let(:parameters) { described_class::Parameters.new(parameters_hash) }
        let(:parameters_hash) { Hash.new }
        let(:signup_persistence) { instance_double(Interfaces::Persistence) }

        describe '#result' do
          subject(:result) { use_case.result }

          let(:validator) do
            instance_double(Validators::UserFillsInStageThreeParameters)
          end

          before do
            expect(Validators::UserFillsInStageThreeParameters)
              .to receive(:new)
              .with(parameters)
              .and_return(validator)
            expect(validator)
              .to receive(:result)
              .and_return(validator_result)
          end

          context 'when the parameters are valid' do
            let(:validator_result) { Dry::Monads::Success(parameters) }
            let(:stage_one_entity) { instance_double(Entities::StageOne) }
            let(:stage_two_entity) { instance_double(Entities::StageTwo) }
            let(:stage_three_entity) { instance_double(Entities::StageThree) }

            before do
              expect(Entities::StageThree)
                .to receive(:new)
                .with(parameters)
                .and_return(stage_three_entity)
              expect(signup_persistence)
                .to receive(:load_stage_one)
                .and_return(Dry::Monads::Success(stage_one_entity))
              expect(signup_persistence)
                .to receive(:load_stage_two)
                .and_return(Dry::Monads::Success(stage_two_entity))
              expect(signup_persistence)
                .to receive(:persist_signup)
                .with(stage_one_entity, stage_two_entity, stage_three_entity)
                .and_return(stage_three_entity)
            end

            it { is_expected.to be_a_success_with_value(stage_three_entity) }
          end

          context 'when the parameters are not valid' do
            let(:validator_result) { Dry::Monads::Failure(errors) }
            let(:errors) { instance_double(Clean::Errors) }

            it { is_expected.to be_a_failure_with_value(errors) }
          end
        end
      end
    end
  end
end
