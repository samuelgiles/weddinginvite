# frozen_string_literal: true

require 'clean/abstract_validator'

module Clean
  describe AbstractValidator do
    class ExampleValidatableObject
      attr_reader :forename

      def initialize(forename)
        @forename = forename
      end
    end

    class ExampleValidator < Clean::AbstractValidator
      performs_validations_on :forename
      validates :forename, presence: true
    end

    let(:validatable_object) { ExampleValidatableObject.new(forename) }
    let(:instance) { ExampleValidator.new(validatable_object) }

    describe '#result' do
      subject(:result) { instance.result }

      context 'when the object is valid' do
        let(:forename) { 'Samuel' }

        it { is_expected.to be_a_success_with_value(validatable_object) }
      end

      context 'when the object is invalid' do
        let(:forename) { nil }

        specify do
          expect(result.failure).to be_an_instance_of(ActiveModel::Errors)
        end
      end
    end
  end
end
