# frozen_string_literal: true

require 'wedding_invite/signup/entities/stage_one'

FactoryBot.define do
  factory :wedding_invite_signup_stage_one, class: WeddingInvite::Signup::Entities::StageOne do
    transient do
      forename { 'Samuel' }
      surname { 'Giles' }
      mobile_phone_number { '+447543651833' }
      partner_forename { 'Stephanie' }
      partner_surname { 'Collins' }
      partner_mobile_phone_number { nil }
    end

    initialize_with do
      attributes = {
        forename: forename,
        surname: surname,
        mobile_phone_number: mobile_phone_number,
        partner_forename: partner_forename,
        partner_surname: partner_surname,
        partner_mobile_phone_number: partner_mobile_phone_number
      }

      WeddingInvite::Signup::Entities::StageOne.new(attributes)
    end
  end
end
