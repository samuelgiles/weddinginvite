# frozen_string_literal: true

require 'app/authentication/forms/login'

module App
  module Authentication
    module Forms
      describe Login do
        let(:form) { described_class.new(params: params) }

        let(:params) do
          WeddingInvite::Authentication::UseCases::UserAttemptsLogin::Parameters.new(
            phone_number: '07543651833'
          )
        end

        before do
          Phonelib.default_country = 'GB'
        end

        # TODO: Test custom parameter object
      end
    end
  end
end
