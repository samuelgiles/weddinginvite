# frozen_string_literal: true

require 'rails_helper'

module App
  describe FormHelper do
    describe '.render_form_errors' do
      subject(:output) { helper.render_form_errors(form) }

      let(:form) { instance_double(Clean::AbstractParameterObjectForm) }
      let(:form_errors) { instance_double(Clean::Errors) }

      before do
        expect(form).to receive(:errors).and_return(form_errors)
        expect(form_errors).to receive(:full_messages).and_return(full_error_messages)
      end

      context 'when there are errors' do
        let(:full_error_messages) do
          [
            'First name should be present',
            'Mobile phone number should be formatted correctly'
          ]
        end
        let(:expected_output) do
          <<-HTML
  <div class="Form__errors">
    <h4 class="Form__errors__heading">We encountered an issue:</h4>
    <ul class="Form__errors__list">
        <li class="Form__errors__list__error">First name should be present</li>
        <li class="Form__errors__list__error">Mobile phone number should be formatted correctly</li>
    </ul>
  </div>
          HTML
        end

        specify do
          expect(output).to eq expected_output
        end
      end

      context 'when there are no errors' do
        let(:full_error_messages) do
          []
        end

        it { is_expected.to be_blank }
      end
    end
  end
end
