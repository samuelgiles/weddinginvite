# frozen_string_literal: true

require 'rails_helper'

module App
  module Signup
    describe StageThreeController do
      describe '#new' do
        subject(:make_request) { get :new }

        let(:stage_three_form) { instance_double(Forms::StageThree) }

        before do
          expect(Forms::StageThree).to receive(:new).and_return(stage_three_form)
        end

        specify do
          make_request
          expect(assigns[:form]).to eq stage_three_form
        end
      end

      describe '#create' do
        subject(:make_request) { post :create, params: params }

        let(:params) do
          {
            app_signup_forms_stage_three: permitted_params
          }
        end
        let(:permitted_params) do
          {
            date: '2018-10-29',
            terms_agreed: '1'
          }
        end
        let(:signup_persistence_factory) { instance_double(Factories::Persistence) }
        let(:signup_persistence) { instance_double(Services::Persistence) }
        let(:user_fills_in_stage_three_use_case) do
          instance_double(WeddingInvite::Signup::UseCases::UserFillsInStageThree)
        end
        let(:user_fills_in_stage_three_parameters) do
          instance_double(WeddingInvite::Signup::UseCases::UserFillsInStageThree::Parameters)
        end
        let(:stage_three_form) { instance_double(Forms::StageThree) }

        before do
          expect(Factories::Persistence)
            .to receive(:new)
            .with(controller.session)
            .and_return(signup_persistence_factory)
          expect(signup_persistence_factory).to receive(:manufacture).and_return(signup_persistence)
          expect(Forms::StageThree)
            .to receive(:new)
            .with(params: permitted_params)
            .and_return(stage_three_form)
          expect(stage_three_form)
            .to receive(:to_parameter_object)
            .and_return(user_fills_in_stage_three_parameters)
          expect(WeddingInvite::Signup::UseCases::UserFillsInStageThree)
            .to receive(:new)
            .with(user_fills_in_stage_three_parameters, signup_persistence)
            .and_return(user_fills_in_stage_three_use_case)
          expect(user_fills_in_stage_three_use_case)
            .to receive(:result)
            .and_return(user_fills_in_stage_three_result)
        end

        context 'when use case result is successful' do
          let(:user_fills_in_stage_three_result) { Dry::Monads::Success(stage_three_entity) }
          let(:stage_three_entity) { instance_double(WeddingInvite::Signup::Entities::StageThree) }

          specify do
            make_request
            expect(response).to redirect_to(app_signup_complete_path)
          end
        end

        context 'when use case result is unsuccessful' do
          let(:user_fills_in_stage_three_result) { Dry::Monads::Failure(errors) }
          let(:errors) { instance_double(Clean::Errors) }
          let(:stage_three_form_with_errors) { instance_double(Forms::StageThree) }

          before do
            expect(stage_three_form)
              .to receive(:with_errors)
              .with(errors)
              .and_return(stage_three_form_with_errors)
          end

          specify do
            make_request
            expect(assigns[:form]).to eq stage_three_form_with_errors
            expect(response).to render_template(:new)
          end
        end
      end
    end
  end
end
