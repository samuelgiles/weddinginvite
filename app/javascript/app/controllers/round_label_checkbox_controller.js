import { Controller } from 'stimulus';

export default class extends Controller {
  static targets = ['input']

  connect() {
    this.updateCheckedClass();
  }

  handleChange() {
    this.updateCheckedClass();
  }

  updateCheckedClass() {
    const isChecked = this.inputTarget.checked;
    this.element.classList.toggle('Form__control__round-label--checked', isChecked);
  }
}
