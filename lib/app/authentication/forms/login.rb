# frozen_string_literal: true

require 'clean/abstract_parameter_object_form'
require 'wedding_invite/authentication/use_cases/user_attempts_login'

module App
  module Authentication
    module Forms
      class Login < Clean::AbstractParameterObjectForm
        acts_as_form_for WeddingInvite::Authentication::UseCases::UserAttemptsLogin::Parameters

        private

        def parameter_object_hash
          { phone_number: Phonelib.parse(@params[:phone_number]).international || '' }
        end
      end
    end
  end
end
