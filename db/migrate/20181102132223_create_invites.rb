# frozen_string_literal: true

class CreateInvites < ActiveRecord::Migration[5.2]
  def change
    create_table :invites do |t|
      t.string :code, null: false
      t.string :notes, null: false
      t.boolean :for_ceremony, null: false, default: false
      t.boolean :for_reception, null: false, default: false
      t.references :wedding, foreign_key: true
      t.datetime :sent_at

      t.timestamps
    end
    add_index :invites, [:code, :wedding_id], unique: true
  end
end
