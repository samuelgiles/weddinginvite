# frozen_string_literal: true

require 'rails_helper'

module App
  module Presenters
    describe Navigation do
      let(:presenter) { described_class.new(current_path) }

      let(:current_path) { '/' }

      describe '#items' do
        subject(:items) { presenter.items }

        specify do
          expect(items[0].path).to eq '/'
          expect(items[0].active?).to be true
          expect(items[0].icon).to eq 'app/home.svg'
          expect(items[0].label).to eq 'Home'

          expect(items[1].path).to eq '/'
          expect(items[1].active?).to be false
          expect(items[1].icon).to eq 'app/invite.svg'
          expect(items[1].label).to eq 'Invites'

          expect(items[2].path).to eq '/settings'
          expect(items[2].active?).to be false
          expect(items[2].icon).to eq 'app/settings.svg'
          expect(items[2].label).to eq 'Settings'
        end

        context 'when the current path is /invites/new' do
          let(:current_path) { '/invites/new' }

          specify do
            expect(items[0].active?).to be false
            expect(items[1].active?).to be true
            expect(items[2].active?).to be false
          end
        end
      end
    end
  end
end
