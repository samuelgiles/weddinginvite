# frozen_string_literal: true

require 'app/signup/factories/stage_one_form'
require 'wedding_invite/signup/interfaces/persistence'

module App
  module Signup
    module Factories
      describe StageOneForm do
        let(:factory) { described_class.new(signup_persistence) }
        let(:signup_persistence) { instance_double(WeddingInvite::Signup::Interfaces::Persistence) }

        describe '#manufacture' do
          subject(:manufacture) { factory.manufacture }

          before do
            expect(signup_persistence)
              .to receive(:load_stage_one)
              .and_return(load_stage_one_result)
          end

          context 'when an existing stage one can be loaded' do
            let(:load_stage_one_result) { Dry::Monads::Success(stage_one_entity) }
            let(:stage_one_entity) { FactoryBot.build(:wedding_invite_signup_stage_one) }
            let(:populated_stage_one_form) { instance_double(Forms::StageOne) }

            before do
              expect(Forms::StageOne)
                .to receive(:new)
                .with(params: {
                    forename: 'Samuel',
                    surname: 'Giles',
                    mobile_phone_number: '+447543651833',
                    partner_forename: 'Stephanie',
                    partner_surname: 'Collins',
                    partner_mobile_phone_number: nil
                  }
                )
                .and_return(populated_stage_one_form)
            end

            it { is_expected.to eq populated_stage_one_form }
          end

          context 'when an existing stage one cannot be loaded' do
            let(:load_stage_one_result) { Dry::Monads::Failure(:no_signup_data) }
            let(:empty_stage_one_form) { instance_double(Forms::StageOne) }

            before do
              expect(Forms::StageOne).to receive(:new).and_return(empty_stage_one_form)
            end

            it { is_expected.to eq empty_stage_one_form }
          end
        end
      end
    end
  end
end
