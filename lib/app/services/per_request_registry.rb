# frozen_string_literal: true

module App
  module Services
    # This helps to move the instantation of implementations of interfaces we use
    # out into a testable class that can use the controller instance if need be,
    # e.g. for cookies, request data, etc.
    class PerRequestRegistry
      def initialize(cookies)
        @cookies = cookies
      end

      def authentication_persistence
        @authentication_persistence ||= Authentication::Services::Persistence.new
      end

      def messaging_service_provider
        @messaging_service_provider ||= Factories::PlivoMessagingServiceProvider.new.manufacture
      end

      def session_manager
        @session_manager ||= Authentication::Services::CookieBasedSessionManager.new(@cookies)
      end
    end
  end
end
