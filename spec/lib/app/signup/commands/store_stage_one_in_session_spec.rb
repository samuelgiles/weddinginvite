# frozen_string_literal: true

require 'app/signup/commands/store_stage_one_in_session'

module App
  module Signup
    module Commands
      describe StoreStageOneInSession do
        let(:command) { described_class.new(session, stage_one_entity) }
        let(:session) { Hash.new }
        let(:stage_one_entity) { FactoryBot.build(:wedding_invite_signup_stage_one) }

        describe '#execute' do
          subject(:execute) { command.execute }

          specify do
            execute
            expect(session[:signup_stage_one]).to eq(
              forename: 'Samuel',
              surname: 'Giles',
              mobile_phone_number: '+447543651833',
              partner_forename: 'Stephanie',
              partner_surname: 'Collins',
              partner_mobile_phone_number: nil
            )
          end
        end
      end
    end
  end
end
