# frozen_string_literal: true

require 'rails_helper'

module App
  module Builders
    describe Wedding do
      let(:builder) { described_class.new(ar_person) }

      let(:ar_person) do
        ::Wedding.new(
          id: 2319,
          name: 'Giles Wedding',
          date: Date.parse('6th July 2019')
        )
      end

      describe '#build' do
        subject(:built_entity) { builder.build }

        specify do
          expect(built_entity).to be_an_instance_of(WeddingInvite::Entities::Wedding)
          expect(built_entity.id).to eq 2319
          expect(built_entity.name).to eq 'Giles Wedding'
          expect(built_entity.date).to eq Date.parse('6th July 2019')
        end
      end
    end
  end
end
