# frozen_string_literal: true

require 'wedding_invite/types'
require 'dry/struct'

module WeddingInvite
  module Signup
    module Entities
      # Entity for representing a populated stage three
      class StageThree < Dry::Struct::Value
        attribute :date, Types::Strict::Date
        attribute :terms_agreed, Types::Strict::Bool
      end
    end
  end
end
