require 'turbolinks'

class ApplicationController < ActionController::Base
  include Turbolinks::Redirection
end
