# frozen_string_literal: true

require 'app/signup/factories/stage_two_form'
require 'wedding_invite/signup/interfaces/persistence'
require 'wedding_invite/signup/entities/stage_one'

module App
  module Signup
    module Factories
      describe StageTwoForm do
        let(:factory) { described_class.new(signup_persistence) }
        let(:signup_persistence) { instance_double(WeddingInvite::Signup::Interfaces::Persistence) }

        describe '#manufacture' do
          subject(:manufacture) { factory.manufacture }

          before do
            expect(signup_persistence)
              .to receive(:load_stage_two)
              .and_return(load_stage_two_result)
            expect(signup_persistence).to receive(:base_domain).and_return('weddinginvite.io')
          end

          context 'when stage two has already been populated' do
            let(:load_stage_two_result) { Dry::Monads::Success(stage_two_entity) }
            let(:stage_two_entity) { FactoryBot.build(:wedding_invite_signup_stage_two) }
            let(:existing_stage_two_form) { instance_double(Forms::StageTwo) }

            before do
              expect(Forms::StageTwo)
                .to receive(:new)
                .with(
                  params: { name: 'Giles Wedding', domain: 'giles-wedding' },
                  context: { base_domain: 'weddinginvite.io' }
                )
                .and_return(existing_stage_two_form)
            end

            it { is_expected.to eq existing_stage_two_form }
          end

          context 'when stage two has not already been populated' do
            let(:load_stage_two_result) { Dry::Monads::Failure(:no_signup_data) }
            let(:stage_one_entity) do
              instance_double(WeddingInvite::Signup::Entities::StageOne)
            end
            let(:prepopulated_stage_two_entity) do
              FactoryBot.build(
                :wedding_invite_signup_stage_two,
                name: 'White Wedding',
                domain: 'white-wedding'
              )
            end
            let(:prepopulated_stage_two_form) { instance_double(Forms::StageTwo) }
            let(:prepopulated_stage_two_factory) do
              instance_double(WeddingInvite::Signup::Factories::PrepopulatedStageTwo)
            end

            before do
              expect(signup_persistence)
                .to receive(:load_stage_one)
                .and_return(Dry::Monads::Success(stage_one_entity))
              expect(WeddingInvite::Signup::Factories::PrepopulatedStageTwo)
                .to receive(:new)
                .with(stage_one_entity)
                .and_return(prepopulated_stage_two_factory)
              expect(prepopulated_stage_two_factory)
                .to receive(:manufacture)
                .and_return(prepopulated_stage_two_entity)
              expect(Forms::StageTwo)
                .to receive(:new)
                .with(
                  params: { name: 'White Wedding', domain: 'white-wedding' },
                  context: { base_domain: 'weddinginvite.io' }
                )
                .and_return(prepopulated_stage_two_form)
            end

            it { is_expected.to eq prepopulated_stage_two_form }
          end
        end
      end
    end
  end
end
