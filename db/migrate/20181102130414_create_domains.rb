class CreateDomains < ActiveRecord::Migration[5.2]
  def change
    create_table :domains do |t|
      t.string :hostname, null: false
      t.references :wedding, foreign_key: true

      t.timestamps
    end
    add_index :domains, :hostname, unique: true
  end
end
