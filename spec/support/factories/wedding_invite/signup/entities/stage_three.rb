# frozen_string_literal: true

require 'wedding_invite/signup/entities/stage_three'

FactoryBot.define do
  factory :wedding_invite_signup_stage_three, class: WeddingInvite::Signup::Entities::StageThree do
    transient do
      date { Date.parse('6th July 2030') }
      terms_agreed { true }
    end

    initialize_with do
      attributes = {
        date: date,
        terms_agreed: terms_agreed
      }

      WeddingInvite::Signup::Entities::StageThree.new(attributes)
    end
  end
end
