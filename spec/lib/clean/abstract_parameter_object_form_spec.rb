# frozen_string_literal: true

require 'clean/abstract_parameter_object_form'
require 'clean/parameter_object'
require 'dry/types'

module Clean
  describe AbstractParameterObjectForm do
    module ExampleTypes
      include Dry::Types.module
    end

    class ExampleParameterObject < Clean::ParameterObject
      attribute :forename, ExampleTypes::Strict::String.meta(omittable: true)
      attribute :surname, ExampleTypes::Strict::String.meta(omittable: true)
    end

    class ExampleParameterObjectForm < AbstractParameterObjectForm
      acts_as_form_for ExampleParameterObject
    end

    let(:instance) { ExampleParameterObjectForm.new(params: params, errors: errors) }
    let(:params) { Hash.new }
    let(:errors) { instance_double(Errors) }

    describe '#persisted?' do
      subject(:persisted?) { instance.persisted? }

      it { is_expected.to be false }
    end

    describe '#to_parameter_object' do
      subject(:to_parameter_object) { instance.to_parameter_object }

      let(:params) do
        { 'my_param' => 'is the nicest param' }
      end
      let(:parameter_object) do
        instance_double(ExampleParameterObject)
      end

      before do
        expect(ExampleParameterObject)
          .to receive(:new)
          .with(my_param: 'is the nicest param')
          .and_return(parameter_object)
      end

      it { is_expected.to eq parameter_object }
    end

    describe '#errors' do
      subject { instance.errors }

      it { is_expected.to eq errors }
    end

    describe '#with_errors' do
      subject(:with_errors) { instance.with_errors(new_errors) }

      let(:new_errors) { instance_double(Errors) }
      let(:form_with_errors) { instance_double(ExampleParameterObjectForm) }
      let(:form_class) { class_double(ExampleParameterObjectForm) }

      before do
        expect(instance).to receive(:class).and_return(form_class)
      end

      context 'when called with errors' do
        before do
          expect(form_class)
            .to receive(:new)
            .with(params: params, errors: new_errors, context: {})
            .and_return(form_with_errors)
        end

        it { is_expected.to eq form_with_errors }
      end

      context 'when called with errors and override params' do
        subject(:with_errors) do
          instance.with_errors(new_errors, override_params: { 'my_param' => 'is overridden' })
        end

        before do
          expect(form_class)
            .to receive(:new)
            .with(params: { 'my_param' => 'is overridden' }, errors: new_errors, context: {})
            .and_return(form_with_errors)
        end

        it { is_expected.to eq form_with_errors }
      end
    end

    describe '#with_error_message' do
      subject(:with_error_message) { instance.with_error_message('Somit broke') }

      let(:errors) { instance_double(Errors) }
      let(:form_with_errors) { instance_double(ExampleParameterObjectForm) }
      let(:form_class) { class_double(ExampleParameterObjectForm) }

      before do
        expect(instance).to receive(:class).and_return(form_class)
        expect(Errors).to receive(:new).with(nil).and_return(errors)
        expect(errors).to receive(:add).with(:base, 'Somit broke')
      end

      before do
        expect(form_class)
          .to receive(:new)
          .with(params: {}, errors: errors, context: {})
          .and_return(form_with_errors)
      end

      it { is_expected.to eq form_with_errors }
    end
  end
end
