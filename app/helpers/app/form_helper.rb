# frozen_string_literal: true

module App
  module FormHelper
    def render_form_errors(form)
      render 'app/forms/errors', errors: form.errors
    end
  end
end
