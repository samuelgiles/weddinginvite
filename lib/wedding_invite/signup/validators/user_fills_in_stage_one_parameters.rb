# frozen_string_literal: true

require 'clean/abstract_validator'

module WeddingInvite
  module Signup
    module Validators
      class UserFillsInStageOneParameters < Clean::AbstractValidator
        performs_validations_on :forename,
                                :surname,
                                :mobile_phone_number,
                                :partner_forename,
                                :partner_surname,
                                :partner_mobile_phone_number

        validates :forename,
                  :surname,
                  :mobile_phone_number,
                  :partner_forename,
                  :partner_surname,
                  presence: true

        validates :mobile_phone_number,
                  :partner_mobile_phone_number, phone: { allow_blank: true }

        validate :ensure_mobile_phone_number_is_available,
                 :ensure_partner_mobile_phone_number_is_available,
                 :ensure_mobile_phone_numbers_are_unique

        private

        def ensure_mobile_phone_number_is_available
          return true if mobile_phone_number_is_available?(mobile_phone_number)
          errors.add(:mobile_phone_number, "is already in use")
        end

        def ensure_partner_mobile_phone_number_is_available
          return true if mobile_phone_number_is_available?(partner_mobile_phone_number)
          errors.add(:partner_mobile_phone_number, "is already in use")
        end

        def ensure_mobile_phone_numbers_are_unique
          return true unless mobile_phone_number && partner_mobile_phone_number
          return true if mobile_phone_number != partner_mobile_phone_number
          errors.add(:partner_mobile_phone_number, "cannot be the same as your mobile phone number")
        end

        def mobile_phone_number_is_available?(phone_number)
          return true unless phone_number
          signup_persistence.mobile_phone_number_is_available?(phone_number)
        end

        def signup_persistence
          @context.fetch(:signup_persistence)
        end
      end
    end
  end
end
