# frozen_string_literal: true

class AddCeremonyEventToWeddings < ActiveRecord::Migration[5.2]
  def change
    add_reference :weddings, :ceremony_event, foreign_key: { to_table: :events }
  end
end
