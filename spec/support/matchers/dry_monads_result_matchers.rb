# frozen_string_literal: true

RSpec::Matchers.define :be_a_success_with_value do |expected|
  match do |actual_result|
    actual_result.instance_of?(Dry::Monads::Success) &&
      actual_result.value! == expected
  end

  failure_message do |actual_result|
    "expected that #{actual_result} would be a Success with a value of #{expected}"
  end
end

RSpec::Matchers.define :be_a_failure_with_value do |expected|
  match do |actual_result|
    actual_result.instance_of?(Dry::Monads::Failure) &&
      actual_result.failure == expected
  end

  failure_message do |actual_result|
    "expected that #{actual_result} would be a Failure with a value of #{expected}"
  end
end

RSpec::Matchers.define :be_some_value do |expected|
  match do |actual_result|
    actual_result.instance_of?(Dry::Monads::Maybe::Some) &&
      actual_result.value! == expected
  end

  failure_message do |actual_result|
    "expected that #{actual_result} would be a Some with a value of #{expected}"
  end
end

RSpec::Matchers.define :be_a_none do |expected|
  match do |actual_result|
    actual_result.instance_of?(Dry::Monads::Maybe::None)
  end

  failure_message do |actual_result|
    "expected that #{actual_result} would be a None"
  end
end
