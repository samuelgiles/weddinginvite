# frozen_string_literal: true

module App
  class BaseController < ApplicationController
    layout 'app'

    private

    def registry
      @registry ||= Services::PerRequestRegistry.new(cookies)
    end

    delegate :authentication_persistence,
             :messaging_service_provider,
             :session_manager,
             to: :registry

    def authentication_helpers
      @authentication_helpers ||= Services::AuthenticationHelpers.new(session_manager)
    end

    delegate :current_user,
             :current_wedding,
             :logged_in?,
             to: :authentication_helpers

    def self.requires_user_to_be_logged_in
      before_action :redirect_to_login_if_signed_out
    end

    def redirect_to_login_if_signed_out
      return true if logged_in?
      flash[:error] = 'You need to be logged in to continue'
      redirect_to new_app_session_path
    end

    def self.uses_navigation_screen
      before_action :prepare_navigation
    end

    def prepare_navigation
      @navigation = Presenters::Navigation.new(request.path)
    end
  end
end
