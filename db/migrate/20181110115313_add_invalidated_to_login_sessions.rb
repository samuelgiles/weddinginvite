# frozen_string_literal: true

class AddInvalidatedToLoginSessions < ActiveRecord::Migration[5.2]
  def change
    add_column :login_sessions, :invalidated, :boolean, default: false, null: false
    add_index :login_sessions, :invalidated
  end
end
