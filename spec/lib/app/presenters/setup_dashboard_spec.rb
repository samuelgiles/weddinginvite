# frozen_string_literal: true

require 'rails_helper'

module App
  module Presenters
    describe SetupDashboard do
      let(:presenter) { described_class.new(wedding) }

      let(:wedding) { FactoryBot.build(:wedding_invite_wedding) }
      let(:now) { Time.at(1541964600) }

      around do |example|
        Timecop.freeze(now) do
          example.run
        end
      end

      describe '#wedding_name' do
        subject(:wedding_name) { presenter.wedding_name }

        it { is_expected.to eq 'Giles Wedding' }
      end

      describe '#formatted_wedding_date' do
        subject(:formatted_wedding_date) { presenter.formatted_wedding_date }

        it { is_expected.to eq 'Saturday, 6th July 2019' }
      end

      describe '#days_until_wedding' do
        subject(:days_until_wedding) { presenter.days_until_wedding }

        it { is_expected.to eq 238 }
      end

      describe '#steps_completed' do
        subject(:steps_completed) { presenter.steps_completed }

        # TODO: Implement
        it { is_expected.to eq 2 }
      end

      describe '#number_of_steps' do
        subject(:number_of_steps) { presenter.number_of_steps }

        it { is_expected.to eq 3 }
      end

      describe '#picture_uploaded?' do
        subject(:picture_uploaded?) { presenter.picture_uploaded? }

        # TODO: Implement
        it { is_expected.to be true }
      end

      describe '#venue_details_provided?' do
        subject(:venue_details_provided?) { presenter.venue_details_provided? }

        # TODO: Implement
        it { is_expected.to be false }
      end

      describe '#theme_chosen?' do
        subject(:theme_chosen?) { presenter.theme_chosen? }

        # TODO: Implement
        it { is_expected.to be true }
      end
    end
  end
end
