# frozen_string_literal: true

require 'wedding_invite/signup/entities/stage_two'

FactoryBot.define do
  factory :wedding_invite_signup_stage_two, class: WeddingInvite::Signup::Entities::StageTwo do
    transient do
      name { 'Giles Wedding' }
      domain { 'giles-wedding' }
    end

    initialize_with do
      attributes = {
        name: name,
        domain: domain
      }

      WeddingInvite::Signup::Entities::StageTwo.new(attributes)
    end
  end
end
